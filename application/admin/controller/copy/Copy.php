<?php

namespace app\admin\controller\copy;

use app\common\controller\Backend;
use app\admin\controller\CopyClass;

/**
 * 复制
 *
 * @icon fa fa-copy
 */
class Copy extends Backend
{
    
    /**
     * Copy模型对象
     * @var \app\admin\model\copy\Copy
     */
    protected $model = null;

    public function _initialize()
    {
        parent::_initialize();
        $this->model = new \app\admin\model\copy\Copy;
        $this->view->assign("typeList", $this->model->getTypeList());
        $this->view->assign("isCopyTypeList", $this->model->getIsCopyTypeList());
    }

    /**
     * 查看
     */
    public function index()
    {
        //当前是否为关联查询
        $this->relationSearch = false;
        //设置过滤方法
        $this->request->filter(['strip_tags']);
        if ($this->request->isAjax())
        {
            //如果发送的来源是Selectpage，则转发到Selectpage
            if ($this->request->request('keyField'))
            {
                return $this->selectpage();
            }
            list($where, $sort, $order, $offset, $limit) = $this->buildparams();
            $total = $this->model
                    
                    ->where($where)
                    ->order($sort, $order)
                    ->count();

            $list = $this->model
                    
                    ->where($where)
                    ->order($sort, $order)
                    ->limit($offset, $limit)
                    ->select();

            foreach ($list as $row) {
                $row->visible(['id','type','is_copy_type']);
                
            }
            $list = collection($list)->toArray();
            $result = array("total" => $total, "rows" => $list);

            return json($result);
        }
        return $this->view->fetch();
    }

    public function add()
    {
        if ($this->request->isPost()) {
            $data = $this->request->post("row/a");
            if(!isset($data['type']) || !isset($data['type'])){
                $this->error('没有复制类型');
            }

            if(!isset($data['beforeLanguage']) || !isset($data['afterLanguage']) || empty($data['beforeLanguage']) || empty($data['afterLanguage'])){
                $this->error('没有选择语言');
            }
            $beforeLanguage = $data['beforeLanguage'];
            $afterLanguage = $data['afterLanguage'];
            $type = $data['type'];

            if($beforeLanguage == $afterLanguage){
                $this->error('复制的语言和被复制语言不能是一样');
            }

            if($type == 1){
                $res = (new CopyClass())->copyNews();
                if($res['code'] == 0){
                    $this->error($res['msg']);
                }else{
                    $this->success($res['msg']);
                }
            }elseif($type == 2){
                $res = (new CopyClass())->copyProduct();
                if($res['code'] == 0){
                    $this->error($res['msg']);
                }else{
                    $this->success($res['msg']);
                }
            }

        }
        return $this->view->fetch();
    }



}
