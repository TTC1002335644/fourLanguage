<?php

namespace app\admin\controller\product;

use app\common\controller\Backend;
use think\Request;

/**
 * 产品分类和颜色关联管理
 *
 * @icon fa fa-circle-o
 */
class CategoryColorRelation extends Backend
{



    
    /**
     * CategoryColorRelation模型对象
     * @var \app\admin\model\product\CategoryColorRelation
     */
    protected $model = null;

    public function _initialize()
    {
        parent::_initialize();
        $this->model = new \app\admin\model\product\CategoryColorRelation;

    }

    /**
     * Selectpage搜索
     *
     * @internal
     */
    public function selectpage(array $custom = [])
    {
        $custom = (array)$this->request->request("custom/a");
        if(!isset($custom['language_id']) || !isset($custom['product_category_id']) ){
//            return ['list' => []];
        }
        if(isset($custom['product_category_id'])){
            $data = db('product_category')->where(['id' => $custom['product_category_id'] ])->value('pid');
            if(!empty($data)){
                $res = (new ProductCategory())->getTopParent($data);
                if(!empty($res)){
                    $custom['product_category_id'] = ['in' , [$custom['product_category_id'],$res]];
                }
            }
        }
        return parent::selectpage($custom);
    }


    /**
     * 写入参数
     * @param int $language_id
     * @param int $category_id
     * @param string $string
     */
    public function insertAttribute(int $language_id , int $category_id  , string $string){
        $arr = explode("\n" , $string);
        if(!empty($arr)){
            $time = time();
            $data = [];
            foreach($arr as $k => $v){
                if(!empty(trim($v))){
                    $data[] = [
                        'language_id' => $language_id,
                        'product_category_id' => $category_id,
                        'color_name' => trim($v),
                        'createtime' => $time,
                    ];
                }
            }

            if(!empty($data)){
                db('category_color_relation')->insertAll($data);
            }
        }
    }



    /**
     * 修改属性
     * @param int $category_id
     * @param array $attribute_ids
     * @param array $is_dels
     * @param array $attribute_names
     * @return bool
     * @throws \think\Exception
     * @throws \think\exception\PDOException
     */
    public function updateAttribute(int $category_id = 0 , $attribute_ids = [] , $is_dels = [] , $attribute_names = []){
        if(empty($category_id) || empty($attribute_ids) || empty($is_dels) || empty($attribute_names) || !is_array($attribute_ids) || !is_array($is_dels) || !is_array($attribute_names)){
            return false;
        }

        foreach ($attribute_ids as $k => $v){
            if($is_dels[$k] == 1){
                db('category_color_relation')->where(['id' =>$v ,'product_category_id' => $category_id])->delete();
            }else{
                db('category_color_relation')->where(['id' =>$v ,'product_category_id' => $category_id])->update(['color_name' => $attribute_names[$k]]);
            }
        }

        return true;

    }


    /**
     * 删除
     * @param int $language_id
     * @param int $category_id
     * @throws \think\Exception
     * @throws \think\exception\PDOException
     */
    public function deleteAttribute(int $language_id = 0, int $category_id = 0 ){
        db('category_color_relation')->where(['language_id ' =>$language_id ,'product_category_id' => $category_id])->delete();
    }



    /**
     * 用于产品列表筛选
     * @return \think\response\Json
     */
    public function getColorCategory($category_id = 0){
        //先获取他的顶级分类id
        $current = db('product_category')->where(['id' =>$category_id ])->find();
        if(empty($current)){
            return json([]);
        }
        if($current['pid'] == 0){
            $topPid = $category_id;
        }else{
            $topPid = (new ProductCategory())->getTopParent($current['pid']);
        }
        $res = db('category_color_relation')
            ->where('product_category_id',$topPid)
            ->order('weigh' , 'asc')
            ->select();
        return json($res);
    }



}
