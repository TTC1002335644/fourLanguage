<?php

namespace app\admin\controller\product;

use app\common\controller\Backend;
use fast\Tree;
use think\Db;
use think\Exception;
use think\exception\PDOException;
use think\exception\ValidateException;


/**
 * 产品分类
 *
 * @icon fa fa-circle-o
 */
class ProductCategory extends Backend
{
    
    /**
     * ProductCategory模型对象
     * @var \app\admin\model\product\ProductCategory
     */
    protected $model = null;

    protected $categorylist = [];

    protected $noNeedRight = ['selectpage'];


    protected $searchFields = 'name';

    protected $multiFields="is_open,is_recommend";

    public function _initialize()
    {
        parent::_initialize();
        $this->model = new \app\admin\model\product\ProductCategory;
        $this->view->assign("flagList", $this->model->getFlagList());
        $this->view->assign("isOpenList", $this->model->getIsOpenList());
        $this->view->assign("isRecommendList", $this->model->getIsRecommendList());


        $tree = Tree::instance();
        $tree->init(collection($this->model->order('weigh asc')->where(['language_id' => $this->paramLanguageId])->select())->toArray(), 'pid');
        $this->categorylist = $tree->getTreeList($tree->getTreeArray(0), 'name');
        $categorydata = [0 => ['type' => 'all', 'name' => __('None')]];
        foreach ($this->categorylist as $k => $v) {
            $categorydata[$v['id']] = $v;
        }
        $this->view->assign("parentList", $categorydata);
    }

    public function treeList(){
            //设置过滤方法
          $this->request->filter(['strip_tags']);
          if ($this->request->isAjax()) {
              //如果发送的来源是Selectpage，则转发到Selectpage
              if ($this->request->request('keyField')) {
                  return $this->selectpage();
              }
          }
     }

    /**
     * 查看
     */
    public function index()
    {
        //设置过滤方法
        $this->request->filter(['strip_tags']);
        if ($this->request->isAjax()) {
            if ($this->request->request('keyField')){
                return $this->selectpage();
            }

            $search = $this->request->request("search");
            $type = $this->request->request("type");

            //构造父类select列表选项数据
            $list = [];

            foreach ($this->categorylist as $k => $v) {
                if ($search) {
                    if ($v['type'] == $type && stripos($v['name'], $search) !== false || stripos($v['nickname'], $search) !== false) {
                        if ($type == "all" || $type == null) {
                            $list = $this->categorylist;
                        } else {
                            $list[] = $v;
                        }
                    }
                } else {
                    if ($type == "all" || $type == null) {
                        $list = $this->categorylist;
                    } elseif ($v['type'] == $type) {
                        $list[] = $v;
                    }
                }
            }

            $total = count($list);
            $result = array("total" => $total, "rows" => $list);

            return json($result);
        }
        return $this->view->fetch();
    }




    /**
     * 添加
     */
    public function add()
    {
        if ($this->request->isPost()) {
            $params = $this->request->post("row/a");
            if ($params) {
                $params = $this->preExcludeFields($params);
                $params['language_id'] = $this->paramLanguageId;
                if ($this->dataLimit && $this->dataLimitFieldAutoFill) {
                    $params[$this->dataLimitField] = $this->auth->id;
                }
                $result = false;
                Db::startTrans();
                try {
                    //是否采用模型验证
                    if ($this->modelValidate) {
                        $name = str_replace("\\model\\", "\\validate\\", get_class($this->model));
                        $validate = is_bool($this->modelValidate) ? ($this->modelSceneValidate ? $name . '.add' : $name) : $this->modelValidate;
                        $this->model->validateFailException(true)->validate($validate);
                    }

                    if(!empty($params['pid'])){
//                        $params['introduction'] = '';
                    }

                    $result = $this->model->allowField(true)->save($params);

                    if($result !== false){
                        if(empty($params['pid'])){
                            $ids = $this->model->id;
                            $language_id = $this->paramLanguageId;
                            $attribute_string = $params['attribute'];
                            $color_string = $params['color'];
                            (new CategoryAttributeRelation())->insertAttribute($language_id , $ids , $attribute_string);
                            (new CategoryColorRelation())->insertAttribute($language_id , $ids , $color_string);
                        }

                    }
                    Db::commit();
                } catch (ValidateException $e) {
                    Db::rollback();
                    $this->error($e->getMessage());
                } catch (PDOException $e) {
                    Db::rollback();
                    $this->error($e->getMessage());
                } catch (Exception $e) {
                    Db::rollback();
                    $this->error($e->getMessage());
                }
                if ($result !== false) {
                    $this->success();
                } else {
                    $this->error(__('No rows were inserted'));
                }
            }
            $this->error(__('Parameter %s can not be empty', ''));
        }
        return $this->view->fetch();
    }



    /**
     * 编辑
     */
    public function edit($ids = null)
    {

        $row = $this->model->get($ids);
        if (!$row) {
            $this->error(__('No Results were found'));
        }
        $adminIds = $this->getDataLimitAdminIds();
        if (is_array($adminIds)) {
            if (!in_array($row[$this->dataLimitField], $adminIds)) {
                $this->error(__('You have no permission'));
            }
        }
        if ($this->request->isPost()) {
            $params = $this->request->post("row/a");
            if ($params) {
                $params = $this->preExcludeFields($params);
                $result = false;
                Db::startTrans();
                try {
                    //是否采用模型验证
                    if ($this->modelValidate) {
                        $name = str_replace("\\model\\", "\\validate\\", get_class($this->model));
                        $validate = is_bool($this->modelValidate) ? ($this->modelSceneValidate ? $name . '.edit' : $name) : $this->modelValidate;
                        $row->validateFailException(true)->validate($validate);
                    }

                    if($params['pid'] == $ids){
                        $this->error(__('Parent category can not be self'));
                    }

                    $language_id = $row->language_id;

                    if(!empty($params['pid'])){
//                        $params['introduction'] = '';
                    }

                    $result = $row->allowField(true)->save($params);

                    if($result !== false){
                        $CategoryAttributeRelation = new CategoryAttributeRelation();
                        $CategoryColorRelation = new CategoryColorRelation();
                        if(empty($params['pid'])){
                            $attribute_string = $params['attribute'];
                            $color_string = $params['color'];

                            //长度或重量
                            $CategoryAttributeRelation->insertAttribute($language_id , $ids , $attribute_string);
                            if(isset($params['attribute_id']) && isset($params['is_del']) && isset($params['attribute_name']) && isset($params['sort']) ){
                                $CategoryAttributeRelation->updateAttribute($ids , $params['attribute_id'] ,  $params['is_del'] ,  $params['attribute_name'] , $params['sort'] );
                            }
                            //颜色
                            $CategoryColorRelation->insertAttribute($language_id , $ids , $color_string);
                            if(isset($params['color_id']) && isset($params['is_del_color']) && isset($params['color_name']) ){
                                $CategoryColorRelation->updateAttribute($ids , $params['color_id'] ,  $params['is_del_color'] ,  $params['color_name']);
                            }
                        }else{
                            $CategoryAttributeRelation->deleteAttribute($language_id , $ids);
                            $CategoryColorRelation->deleteAttribute($language_id , $ids);
                        }

                    }

                    Db::commit();
                } catch (ValidateException $e) {
                    Db::rollback();
                    $this->error($e->getMessage());
                } catch (PDOException $e) {
                    Db::rollback();
                    $this->error($e->getMessage());
                } catch (Exception $e) {
                    Db::rollback();
                    $this->error($e->getMessage());
                }
                if ($result !== false) {
                    $this->success();
                } else {
                    $this->error(__('No rows were updated'));
                }
            }
            $this->error(__('Parameter %s can not be empty', ''));
        }
        //获取关联的属性
        $option = ['product_category_id' => $ids];
        $row->attribute = db('category_attribute_relation')->where($option)->order('weigh' , 'asc')->order('id' , 'asc')->select();
        $row->color = db('category_color_relation')->where($option)->order('weigh' , 'asc')->order('id' , 'asc')->select();

        if(!empty($row->pid)){
//            $row->introduction = '';
        }


        $this->view->assign("row", $row);
        return $this->view->fetch();
    }



    /**
     * 删除
     */
    public function del($ids = "")
    {
        if ($ids) {
            $pk = $this->model->getPk();
            $adminIds = $this->getDataLimitAdminIds();
            if (is_array($adminIds)) {
                $this->model->where($this->dataLimitField, 'in', $adminIds);
            }
            $list = $this->model->where($pk, 'in', $ids)->select();

            $count = 0;
            Db::startTrans();
            try {
                $CategoryAttributeRelation = new CategoryAttributeRelation();
                $CategoryColorRelation = new CategoryColorRelation();
                foreach ($list as $k => $v) {
                    //先删除属性
                    $CategoryAttributeRelation->deleteAttribute($v->language_id , $v->id);
                    $CategoryColorRelation->deleteAttribute($v->language_id , $v->id);
                    $count += $v->delete();
                }
                Db::commit();
            } catch (PDOException $e) {
                Db::rollback();
                $this->error($e->getMessage());
            } catch (Exception $e) {
                Db::rollback();
                $this->error($e->getMessage());
            }
            if ($count) {
                $this->success();
            } else {
                $this->error(__('No rows were deleted'));
            }
        }
        $this->error(__('Parameter %s can not be empty', 'ids'));
    }


    /**
     * 获取顶级分类id
     * @param int $pid
     * @return int
     */
    public function getTopParent($pid = 0){
        if(empty($pid)){
            return $pid;
        }
        $res = db('product_category')->where(['id' => $pid])->field(['pid' , 'id'])->find();
        if(!empty($res)){
            if(empty($res['pid'])){
                return $res['id'];
            }else{
                return $this->getTopParent($res['pid']);
            }
        }else{
            return 0;
        }
    }


    /**
     * 用于产品列表筛选
     * @return \think\response\Json
     */
    public function getSecondCategory($language_id = 0 , $pid = 0){
        $res = db('product_category')
            ->where('pid',$pid)
            ->where(['language_id' => $language_id])
            ->order('pid' , 'asc')
            ->order('weigh' , 'asc')
            ->select();
        return json($res);
    }



}
