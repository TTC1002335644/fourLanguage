<?php

namespace app\admin\controller\product;

use app\admin\model\product\ProductColorRelation;
use app\common\controller\Backend;
use think\Db;
use think\Exception;
use think\exception\PDOException;
use think\exception\ValidateException;

/**
 * 产品管理管理
 *
 * @icon fa fa-circle-o
 */
class Product extends Backend
{
    
    /**
     * Product模型对象
     * @var \app\admin\model\product\Product
     */
    protected $model = null;

    protected $relationSearch = true;

    protected $searchFields = ['title' , 'color.color_id'];

    public function _initialize()
    {
        parent::_initialize();
        $this->model = new \app\admin\model\product\Product;
        $this->view->assign("isOpenList", $this->model->getIsOpenList());
    }


    /**
     * 列表
     * @return string|\think\response\Json
     * @throws Exception
     * @throws \think\db\exception\DataNotFoundException
     * @throws \think\db\exception\ModelNotFoundException
     * @throws \think\exception\DbException
     */
    public function index()
    {
        $with = [
            'category' => function($query){
                $query->field(['id' , 'name']);
            },
            'Color' => function($query){
            },
            'Weights' => function($query){
            },
        ];
        //设置过滤方法
        $this->request->filter(['strip_tags']);
        if ($this->request->isAjax()) {
            //如果发送的来源是Selectpage，则转发到Selectpage
            if ($this->request->request('keyField')) {
                return $this->selectpage();
            }
            list($where, $sort, $order, $offset, $limit) = $this->buildparams();

//            dump(json_decode($this->request->get("filter", ''),true));
            $filter = json_decode($this->request->get("filter", ''),true);
            $elseWhere = [];
            if(isset($filter['none'])){
                //查找二级的分类
                $elseWhere = db('product_category')->where('pid',$filter['none'])->column('id');
            }




            $total = $this->model
                ->alias('product')
                ->where(function($query) use($elseWhere , $filter){
                    if(isset($filter['none'])){
                        $query->where(['language_id' => $this->paramLanguageId])->whereIn('product_category',$elseWhere);
                    }else{
                        $query->where(['language_id' => $this->paramLanguageId]);
                    }
                })
                ->where($where)
                ->with($with)
                ->order($sort, $order)
                ->group('`product`.`id`')
                ->count();

            $list = $this->model
                ->alias('product')
                ->where(function($query) use($elseWhere , $filter){
                    if(isset($filter['none'])){
                        $query->where(['language_id' => $this->paramLanguageId])->whereIn('product_category',$elseWhere);
                    }else{
                        $query->where(['language_id' => $this->paramLanguageId]);
                    }
                })
                ->where($where)
                ->with($with)
                ->order($sort, $order)
                ->limit($offset, $limit)
                ->group('`product`.`id`')
                ->select();

            $list = collection($list)->toArray();
            $result = array("total" => $total, "rows" => $list);

            return json($result);
        }
        return $this->view->fetch();
    }



    /**
     * 添加
     * @return string
     * @throws \think\Exception
     */
    public function add()
    {
        if ($this->request->isPost()) {
            $params = $this->request->post("row/a");
            if ($params) {
                $params = $this->preExcludeFields($params);

                if ($this->dataLimit && $this->dataLimitFieldAutoFill) {
                    $params[$this->dataLimitField] = $this->auth->id;
                }
                $result = false;
                Db::startTrans();
                try {
                    //是否采用模型验证
                    if ($this->modelValidate) {
                        $name = str_replace("\\model\\", "\\validate\\", get_class($this->model));
                        $validate = is_bool($this->modelValidate) ? ($this->modelSceneValidate ? $name . '.add' : $name) : $this->modelValidate;
                        $this->model->validateFailException(true)->validate($validate);
                    }
                    $params['language_id'] = $this->paramLanguageId;
//                    $params['attribute'] = handleAttributesString($params['attribute'] , false);
                    if(!empty(db('config')->where('name','handleAttributes')->value('value'))){
                        $params['attribute'] = handleAttributesString($params['attribute'] , false);
                    }
//                    $params['attribute'] = json_encode(handleDataAttributes($params['attribute-name'] , $params['attribute-value'] ));
                    $result = $this->model->allowField(true)->save($params);

                    if($result !== false){
                        $id = $this->model->id;
//                        (new ProductDescription())->updateNewsDescription($id , $params['content'] , $params['seo_title'] , $params['seo_keywork'] , $params['seo_description']);
                        (new ProductDescription())->updateNewsDescription($id , $params['content']);

                        /***写入颜色*/
                        $colorArr = explode(',' , $params['color']);
                        if(!empty($colorArr)){
                            $colorRelationData = array_map(function($v) use($id){
                                return [
                                    'product_id' => $id,
                                    'color_id' => $v,
                                ];
                            } , $colorArr);
                            (new ProductColorRelation())->insertAll($colorRelationData);
                        }

                    }

                    Db::commit();
                } catch (ValidateException $e) {
                    Db::rollback();
                    $this->error($e->getMessage());
                } catch (PDOException $e) {
                    Db::rollback();
                    $this->error($e->getMessage());
                } catch (Exception $e) {
                    Db::rollback();
                    $this->error($e->getMessage());
                }
                if ($result !== false) {
                    $this->success();
                } else {
                    $this->error(__('No rows were inserted'));
                }
            }
            $this->error(__('Parameter %s can not be empty', ''));
        }
        return $this->view->fetch();
    }

    /**
     * 编辑
     * @param null $ids
     * @return string
     * @throws Exception
     * @throws \think\exception\DbException
     */
    public function edit($ids = null)
    {
        $with = [
            'description',
        ];
        $row = $this->model->get($ids , $with);
        if (!$row) {
            $this->error(__('No Results were found'));
        }

        $adminIds = $this->getDataLimitAdminIds();
        if (is_array($adminIds)) {
            if (!in_array($row[$this->dataLimitField], $adminIds)) {
                $this->error(__('You have no permission'));
            }
        }
        if ($this->request->isPost()) {
            $params = $this->request->post("row/a");
            if ($params) {
                $params = $this->preExcludeFields($params);
                $result = false;
                Db::startTrans();
                try {
                    //是否采用模型验证
                    if ($this->modelValidate) {
                        $name = str_replace("\\model\\", "\\validate\\", get_class($this->model));
                        $validate = is_bool($this->modelValidate) ? ($this->modelSceneValidate ? $name . '.edit' : $name) : $this->modelValidate;
                        $row->validateFailException(true)->validate($validate);
                    }
//                    $params['attribute'] = json_encode(handleDataAttributes($params['attribute-name'] , $params['attribute-value'] ));
                    if(!empty(db('config')->where('name','handleAttributes')->value('value'))){
                        $params['attribute'] = handleAttributesString($params['attribute'] , false);
                    }
//                    unset($params['attribute-name'] , $params['attribute-value']);
                    unset($params['language_id']);
                    $result = $row->allowField(true)->save($params);
                    if($result !== false){
                        $id = $ids;
//                        (new ProductDescription())->updateNewsDescription($id , $params['content'] , $params['seo_title'] , $params['seo_keywork'] , $params['seo_description']);
                        (new ProductDescription())->updateNewsDescription($id , $params['content'] );

                        $colorArr = explode(',' , $params['color']);

                        (new ProductColorRelation())->where(['product_id' => $ids])->delete();

                        if(!empty($colorArr)){
                            $colorRelationData = array_map(function($v) use($id){
                                return [
                                    'product_id' => $id,
                                    'color_id' => $v,
                                ];
                            } , $colorArr);

                            (new ProductColorRelation())->allowField(true)->insertAll($colorRelationData);
                        }

                    }

                    Db::commit();
                } catch (ValidateException $e) {
                    Db::rollback();
                    $this->error($e->getMessage());
                } catch (PDOException $e) {
                    Db::rollback();
                    $this->error($e->getMessage());
                } catch (Exception $e) {
                    Db::rollback();
                    $this->error($e->getMessage());
                }
                if ($result !== false) {
                    $this->success();
                } else {
                    $this->error(__('No rows were updated'));
                }
            }
            $this->error(__('Parameter %s can not be empty', ''));
        }

        /**获取关联的颜色**/
        $colorRelationArr = (new ProductColorRelation())->where(['product_id' => $ids])->field(['color_id'])->select();
        if(!empty($colorRelationArr)){
            $row->color = implode(',' , array_column($colorRelationArr , 'color_id'));
        }

//        $row->attribute = json_decode($row->attribute , true);
        $this->view->assign("row", $row);
        return $this->view->fetch();
    }


    /**
     * 删除
     */
    public function del($ids = "")
    {
        if ($ids) {
            $pk = $this->model->getPk();
            $adminIds = $this->getDataLimitAdminIds();
            if (is_array($adminIds)) {
                $this->model->where($this->dataLimitField, 'in', $adminIds);
            }
            $list = $this->model->where($pk, 'in', $ids)->select();

            $count = 0;
            Db::startTrans();
            try {
                foreach ($list as $k => $v) {
                    $count += $v->delete();
                }
                Db::commit();
            } catch (PDOException $e) {
                Db::rollback();
                $this->error($e->getMessage());
            } catch (Exception $e) {
                Db::rollback();
                $this->error($e->getMessage());
            }
            if ($count) {
                //删除详情、颜色关联
                db('product_color_relation')->where('product_id' , 'in' , $ids)->delete();
                db('product_description')->where('product_id' , 'in' , $ids)->delete();

                $this->success();
            } else {
                $this->error(__('No rows were deleted'));
            }
        }
        $this->error(__('Parameter %s can not be empty', 'ids'));
    }


    /**
     * 复制
     * @param null $ids
     * @return string
     * @throws Exception
     */
    public function copy($ids = null){
        if ($this->request->isPost()) {
            $row = $this->model->get($ids);
            $params = $this->request->post("row/a");

            if (!$row) {
                $this->error(__('No Results were found'));
            }
            if(!isset($params['afterLanguage']) || !isset($params['product_category']) || !isset($params['weight']) || !isset($params['color']) || empty($params['afterLanguage']) || empty($params['product_category']) || empty($params['weight']) || empty($params['color']) ){
                $this->error(__('参数异常'));
            }


            Db::startTrans();
            try {
                /** 1、先复制基础信息 **/
                $base = db('product')->where(['id' => $ids])->find();

                $base['copy_id'] = $base['id'];
                $base['language_id'] = $params['afterLanguage'];
                $base['product_category'] = $params['product_category'];
                $base['weight'] = $params['weight'];
                unset($base['id']);

                $productId = db('product')->insertGetId($base);


                /** 2、复制详细信息 **/
                $detail = db('product_description')->where('product_id' , $ids)->field('description_id' , true)->find();
                if(!empty($detail)){
                    $detail['copy_id'] = $detail['product_id'];
                    $detail['product_id'] = $productId;
                    db('product_description')->insert($detail);
                }

                /** 3、复制产品颜色关联表 **/
                if(!empty($params['color'])){
                    $relationData = [];
                    $colorArr = explode(',' , $params['color']);
                    foreach($colorArr as $v){
                        $relationData[] = [
                            'copy_id' => 0,
                            'copy_product_id' => $ids,
                            'product_id' => $productId,
                            'color_id' => $v
                        ];
                    }

                    db('product_color_relation')->insertAll($relationData);
                }

                Db::commit();
            }catch (PDOException $e) {
                Db::rollback();
                $this->error($e->getMessage());
            } catch (Exception $e) {
                Db::rollback();
                $this->error($e->getMessage());
            }
            $this->success();

        }


        return $this->view->fetch();

    }

}
