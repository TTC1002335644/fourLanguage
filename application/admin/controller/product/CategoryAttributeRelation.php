<?php

namespace app\admin\controller\product;

use app\common\controller\Backend;

/**
 * 产品分类和长度（重量）关联管理
 *
 * @icon fa fa-circle-o
 */
class CategoryAttributeRelation extends Backend
{
    
    /**
     * CategoryAttributeRelation模型对象
     * @var \app\admin\model\product\CategoryAttributeRelation
     */
    protected $model = null;

    public function _initialize()
    {
        parent::_initialize();
        $this->model = new \app\admin\model\product\CategoryAttributeRelation;

    }

    /**
     * Selectpage搜索
     *
     * @internal
     */
    public function selectpage(array $custom = [])
    {
        $custom = (array)$this->request->request("custom/a");
        if(!isset($custom['language_id']) || !isset($custom['product_category_id']) ){
//            return ['list' => []];
        }

        if(isset($custom['product_category_id'])){
            $data = db('product_category')->where(['id' => $custom['product_category_id'] ])->value('pid');
            if(!empty($data)){
                $res = (new ProductCategory())->getTopParent($data);
                if(!empty($res)){
                    $custom['product_category_id'] = ['in' , [$custom['product_category_id'],$res]];
                }
            }
        }
        return parent::selectpage($custom);
    }


    public function insertAttribute(int $language_id , int $category_id  , string $string){
        $arr = explode("\n" , $string);
        if(!empty($arr)){
            $time = time();
            $data = [];
            foreach($arr as $k => $v){
                if(!empty(trim($v))){
                    $data[] = [
                        'language_id' => $language_id,
                        'product_category_id' => $category_id,
                        'attribute_name' => trim($v),
                        'createtime' => $time,
                    ];
                }
            }

            if(!empty($data)){
                db('category_attribute_relation')->insertAll($data);
            }
        }
    }


    /**
     * 修改属性
     * @param int $category_id
     * @param array $attribute_ids
     * @param array $is_dels
     * @param array $attribute_names
     * @return bool
     * @throws \think\Exception
     * @throws \think\exception\PDOException
     */
    public function updateAttribute(int $category_id = 0 , $attribute_ids = [] , $is_dels = [] , $attribute_names = [] , $weigh = []){
        if(empty($category_id) || empty($attribute_ids) || empty($is_dels) || empty($attribute_names) || !is_array($attribute_ids) || !is_array($is_dels) || !is_array($attribute_names)){
            return false;
        }

        foreach ($attribute_ids as $k => $v){
            if($is_dels[$k] == 1){
                db('category_attribute_relation')->where(['id' =>$v ,'product_category_id' => $category_id])->delete();
            }else{
                db('category_attribute_relation')->where(['id' =>$v ,'product_category_id' => $category_id])->update([
                    'attribute_name' => $attribute_names[$k],
                    'weigh' => $weigh[$k],
                ]);
            }
        }

        return true;

    }


    /**
     * 删除
     * @param int $language_id
     * @param int $category_id
     * @throws \think\Exception
     * @throws \think\exception\PDOException
     */
    public function deleteAttribute(int $language_id = 0, int $category_id = 0 ){
        db('category_attribute_relation')->where(['language_id ' =>$language_id ,'product_category_id' => $category_id])->delete();
    }



    /**
     * 用于产品列表筛选
     * @return \think\response\Json
     */
    public function getAttributeCategory($category_id = 0){
        //先获取他的顶级分类id
        $current = db('product_category')->where(['id' =>$category_id ])->find();
        if(empty($current)){
            return json([]);
        }
        if($current['pid'] == 0){
            $topPid = $category_id;
        }else{
            $topPid = (new ProductCategory())->getTopParent($current['pid']);
        }
        $res = db('category_attribute_relation')
            ->where('product_category_id',$topPid)
            ->order('weigh' , 'asc')
            ->select();
        return json($res);
    }



}
