<?php

namespace app\admin\controller\product;

use app\common\controller\Backend;
use think\Exception;

/**
 * 产品详情
 *
 * @icon fa fa-circle-o
 */
class ProductDescription extends Backend
{

    /**
     * ProductDescription模型对象
     * @var \app\admin\model\product\ProductDescription
     */
    protected $model = null;

    public function _initialize()
    {
        parent::_initialize();
        $this->model = new \app\admin\model\product\ProductDescription;

    }


    /**
     * 添加产品详情
     * @param int $id
     * @param string $content
     * @param string $seo_title
     * @param string $seo_keywork
     * @param string $seo_description
     */
    public function addNewsDescription(int $id , string $content = '' , string $seo_title = '' , string $seo_keywork = '' , string $seo_description = ''){
        try{
            $params = [
                'product_id' => $id,
                'content' => $content,
                'seo_title' => $seo_title,
                'seo_keywork' => $seo_keywork,
                'seo_description' => $seo_description,
            ];
            $result = $this->model->allowField(true)->save($params);
        }catch (Exception $e){
            $this->error($e->getMessage());
        }
    }

    /**
     * 更新产品详情，没有则添加
     * @param int $id
     * @param string $content
     * @param string $seo_title
     * @param string $seo_keywork
     * @param string $seo_description
     */
    public function updateNewsDescription(int $id , string $content = '' , string $seo_title = '' , string $seo_keywork = '' , string $seo_description = ''){
        try{
            $row = $this->model->get(['product_id' => $id]);
            if (!$row) {
                $this->addNewsDescription($id , $content , $seo_title , $seo_keywork , $seo_description );
            }else{
                $params = [
                    'product_id' => $id,
                    'content' => $content,
                    'seo_title' => $seo_title,
                    'seo_keywork' => $seo_keywork,
                    'seo_description' => $seo_description,
                ];
                $result = $row->allowField(true)->save($params);
            }

        }catch (Exception $e){
            $this->error($e->getMessage());
        }
    }


}
