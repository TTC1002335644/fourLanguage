<?php

namespace app\admin\controller\home;

use app\common\controller\Backend;
use think\Db;
use think\Exception;
use think\exception\PDOException;
use think\exception\ValidateException;

/**
 * 轮播图
 *
 * @icon fa fa-circle-o
 */
class Banner extends Backend
{
    
    /**
     * Banner模型对象
     * @var \app\admin\model\home\Banner
     */
    protected $model = null;

    public function _initialize()
    {
        parent::_initialize();
        $this->model = new \app\admin\model\home\Banner;

    }

    /**
     * 查看
     */
    public function index(string  $sort = 'sort' , string $order = 'desc')
    {
        //设置过滤方法
        $this->request->filter(['strip_tags']);
        if ($this->request->isAjax()) {
            //如果发送的来源是Selectpage，则转发到Selectpage
            if ($this->request->request('keyField')) {
                return $this->selectpage();
            }
            list($where, $sort, $order, $offset, $limit) = $this->buildparams();
            $total = $this->model
                ->where(['language_id' => $this->paramLanguageId])
                ->where($where)
                ->order($sort, $order)
                ->count();

            $list = $this->model
                ->where(['language_id' => $this->paramLanguageId])
                ->where($where)
                ->order($sort, $order)
                ->limit($offset, $limit)
                ->select();

            $list = collection($list)->toArray();
            $result = array("total" => $total, "rows" => $list);

            return json($result);
        }
        return $this->view->fetch();
    }

       /**
     * 添加
     * @return string
     * @throws Exception
     */
    public function add()
    {
        if ($this->request->isPost()) {
            $params = $this->request->post("row/a");
            if ($params) {
                $params = $this->preExcludeFields($params);

                if ($this->dataLimit && $this->dataLimitFieldAutoFill) {
                    $params[$this->dataLimitField] = $this->auth->id;
                }
                $result = false;
                Db::startTrans();
                try {
                    //是否采用模型验证
                    if ($this->modelValidate) {
                        $name = str_replace("\\model\\", "\\validate\\", get_class($this->model));
                        $validate = is_bool($this->modelValidate) ? ($this->modelSceneValidate ? $name . '.add' : $name) : $this->modelValidate;
                        $this->model->validateFailException(true)->validate($validate);
                    }
                    if( (isset($params['sort']) && (empty($params['sort']) || $params['sort'] < 0)) || !isset($params['sort']) ){
                        $params['sort'] = $this->getMaxSort($this->paramLanguageId) + 1;
                    }else{
                        $this->updateSort($this->paramLanguageId , $params['sort']);
                    }
                    $params['language_id'] = $this->paramLanguageId;

                    $result = $this->model->allowField(true)->save($params);
                    Db::commit();
                } catch (ValidateException $e) {
                    Db::rollback();
                    $this->error($e->getMessage());
                } catch (PDOException $e) {
                    Db::rollback();
                    $this->error($e->getMessage());
                } catch (Exception $e) {
                    Db::rollback();
                    $this->error($e->getMessage());
                }
                if ($result !== false) {
                    $this->success();
                } else {
                    $this->error(__('No rows were inserted'));
                }
            }
            $this->error(__('Parameter %s can not be empty', ''));
        }
        return $this->view->fetch();
    }

    /**
     * 编辑
     * @param null $ids
     * @return string
     * @throws Exception
     * @throws \think\exception\DbException
     */
    public function edit($ids = null)
    {
        $row = $this->model->get($ids);
        if (!$row) {
            $this->error(__('No Results were found'));
        }
        $adminIds = $this->getDataLimitAdminIds();
        if (is_array($adminIds)) {
            if (!in_array($row[$this->dataLimitField], $adminIds)) {
                $this->error(__('You have no permission'));
            }
        }
        if ($this->request->isPost()) {
            $params = $this->request->post("row/a");
            if ($params) {
                $params = $this->preExcludeFields($params);
                $result = false;
                Db::startTrans();
                try {
                    //是否采用模型验证
                    if ($this->modelValidate) {
                        $name = str_replace("\\model\\", "\\validate\\", get_class($this->model));
                        $validate = is_bool($this->modelValidate) ? ($this->modelSceneValidate ? $name . '.edit' : $name) : $this->modelValidate;
                        $row->validateFailException(true)->validate($validate);
                    }

                    if( (isset($params['sort']) &&  (empty($params['sort']) || $params['sort']) ) || !isset($params['sort']) ){
                        unset($params['sort']);
                    }else{
                        $this->updateSort($this->paramLanguageId , $params['sort'] , $row->sort);
                    }
                    unset($params['language_id']);

                    $result = $row->allowField(true)->save($params);
                    Db::commit();
                } catch (ValidateException $e) {
                    Db::rollback();
                    $this->error($e->getMessage());
                } catch (PDOException $e) {
                    Db::rollback();
                    $this->error($e->getMessage());
                } catch (Exception $e) {
                    Db::rollback();
                    $this->error($e->getMessage());
                }
                if ($result !== false) {
                    $this->success();
                } else {
                    $this->error(__('No rows were updated'));
                }
            }
            $this->error(__('Parameter %s can not be empty', ''));
        }
        $this->view->assign("row", $row);
        return $this->view->fetch();
    }

    /**
     * 获取该语言最大值
     * @param int $language_id
     * @return mixed
     */
    protected function getMaxSort(int $language_id = 0){
        $max = $this->model->where(['language_id' => $language_id])->max('sort');
        return $max;
    }

    /**
     * 让同语言的所有banner的sort 都 加 1
     * @param int $language_id
     * @param int $updateSort
     * @param int $originSort
     * @throws Exception
     */
    protected function updateSort(int $language_id = 0 , int $updateSort = 0 ,  int $originSort = 0){
        $updateSort = ($updateSort < 0) ? 0 : $updateSort;
        $option = [
            'language_id' => $language_id ,
        ];
        if(!empty($originSort)){
            if($updateSort > $originSort){
                $this->model->where($option)->whereBetween('sort' , [$originSort + 1  , $updateSort ])->setDec('sort' , 1);
            }elseif ($updateSort < $originSort){
                $this->model->where($option)->whereBetween('sort' , [$updateSort , $originSort - 1])->setInc('sort' , 1);
            }
        }else{
            $this->model->where($option)->where('sort' , '>' , $updateSort - 1)->setInc('sort' , 1);
        }

    }


    

}
