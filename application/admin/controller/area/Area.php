<?php

namespace app\admin\controller\area;

use app\common\controller\Backend;
use Exception;
use think\Db;
use think\exception\PDOException;
use think\exception\ValidateException;
use fast\Tree;

/**
 * 销售网络
 *
 * @icon fa fa-circle-o
 */
class Area extends Backend
{
    
    /**
     * Area模型对象
     * @var \app\admin\model\area\Area
     */
    protected $model = null;

    public function _initialize()
    {
        parent::_initialize();
        $this->model = new \app\admin\model\area\Area;

    }

    public function treeList(){
        //设置过滤方法
        $this->request->filter(['strip_tags']);
        if ($this->request->isAjax()) {
            //如果发送的来源是Selectpage，则转发到Selectpage
            if ($this->request->request('keyField')) {
                return $this->selectpage();
            }
        }
    }
 

    /**
     * 查看
     */
    public function index()
    {
        //当前是否为关联查询
        $this->relationSearch = false;
        //设置过滤方法
        $this->request->filter(['strip_tags']);
        if ($this->request->isAjax())
        {


            list($where, $sort, $order, $offset, $limit) = $this->buildparams();
            // $total = $this->model
            //         ->where($where)
            //         ->order($sort, $order)
            //         ->count();

            // $list = $this->model
            //         ->where($where)
            //         ->order($sort, $order)
            //         ->limit($offset, $limit)
            //         ->select();
            $tree = Tree::instance();
            $tree->init(collection($this->model->where(['language_id' => $this->paramLanguageId])->order('id asc')->select())->toArray(), 'pid'); 
            $list = $tree->getTreeList($tree->getTreeArray(0), 'name');
            $total = count($list);
            // foreach ($list as $row) {
            //     $row->visible(['id','pid','name','data_json']);
            // }
            // $list = collection($list)->toArray();
            $result = array("total" => $total, "rows" => $list);

            return json($result);
        }
        return $this->view->fetch();
    }



    /**
     * 添加
     */
    public function add()
    {
        if ($this->request->isPost()) {
            $params = $this->request->post("row/a");
            if ($params) {
                $params = $this->preExcludeFields($params);

                if ($this->dataLimit && $this->dataLimitFieldAutoFill) {
                    $params[$this->dataLimitField] = $this->auth->id;
                }
                $result = false;
                Db::startTrans();
                try {
                    //是否采用模型验证
                    if ($this->modelValidate) {
                        $name = str_replace("\\model\\", "\\validate\\", get_class($this->model));
                        $validate = is_bool($this->modelValidate) ? ($this->modelSceneValidate ? $name . '.add' : $name) : $this->modelValidate;
                        $this->model->validateFailException(true)->validate($validate);
                    }
                    $params['language_id'] = $this->paramLanguageId;
                    //处理数据
                    if(isset($params['data_json'])){
                        $params['data_json'] = empty($params['data_json']) ? [] : $params['data_json'];
                        $dealWithData = [];
                        if(isset($params['data_json']['name'])){
                            foreach($params['data_json']['name'] as $k => $v){
                                $dealWithData[] = [
                                    'name' => $v,
//                                    'phone' => $params['data_json']['phone'][$k],
//                                    'mail' => $params['data_json']['mail'][$k],
//                                    'wechat' => $params['data_json']['wechat'][$k],
                                ];
                            }
                        }
                        $params['data_json'] = $dealWithData;
                    }else{
                       $params['data_json'] = []; 
                    }

                    $params['data_json'] = json_encode($params['data_json']);
                    $result = $this->model->allowField(true)->save($params);
                    Db::commit();
                } catch (ValidateException $e) {
                    Db::rollback();
                    $this->error($e->getMessage());
                } catch (PDOException $e) {
                    Db::rollback();
                    $this->error($e->getMessage());
                } catch (Exception $e) {
                    Db::rollback();
                    $this->error($e->getMessage());
                }
                if ($result !== false) {
                    $this->success();
                } else {
                    $this->error(__('No rows were inserted'));
                }
            }
            $this->error(__('Parameter %s can not be empty', ''));
        }
        return $this->view->fetch();
    }



    /**
     * 编辑
     */
    public function edit($ids = null)
    {
        $row = $this->model->get($ids);
        if (!$row) {
            $this->error(__('No Results were found'));
        }
        $adminIds = $this->getDataLimitAdminIds();
        if (is_array($adminIds)) {
            if (!in_array($row[$this->dataLimitField], $adminIds)) {
                $this->error(__('You have no permission'));
            }
        }
        if ($this->request->isPost()) {
            $params = $this->request->post("row/a");
            if ($params) {
                $params = $this->preExcludeFields($params);
                $result = false;
                Db::startTrans();
                try {
                    //是否采用模型验证
                    if ($this->modelValidate) {
                        $name = str_replace("\\model\\", "\\validate\\", get_class($this->model));
                        $validate = is_bool($this->modelValidate) ? ($this->modelSceneValidate ? $name . '.edit' : $name) : $this->modelValidate;
                        $row->validateFailException(true)->validate($validate);
                    }
                    //处理数据
                    if(isset($params['data_json'])){
                        $params['data_json'] = empty($params['data_json']) ? [] : $params['data_json'];
                        $dealWithData = [];
                        if(isset($params['data_json']['name'])){
                            foreach($params['data_json']['name'] as $k => $v){
                                $dealWithData[] = [
                                    'name' => $v,
//                                    'phone' => $params['data_json']['phone'][$k],
//                                    'mail' => $params['data_json']['mail'][$k],
//                                    'wechat' => $params['data_json']['wechat'][$k],
                                ];
                            }
                        }
                        $params['data_json'] = $dealWithData;
                    }else{
                       $params['data_json'] = []; 
                    }

                    $params['data_json'] = json_encode($params['data_json']);

                    $result = $row->allowField(true)->save($params);
                    Db::commit();
                } catch (ValidateException $e) {
                    Db::rollback();
                    $this->error($e->getMessage());
                } catch (PDOException $e) {
                    Db::rollback();
                    $this->error($e->getMessage());
                } catch (Exception $e) {
                    Db::rollback();
                    $this->error($e->getMessage());
                }
                if ($result !== false) {
                    $this->success();
                } else {
                    $this->error(__('No rows were updated'));
                }
            }
            $this->error(__('Parameter %s can not be empty', ''));
        }
        $row->data_json = json_decode($row->data_json , true);
        $this->view->assign("row", $row);
        return $this->view->fetch();
    }


    /**
     * 删除
     */
    public function del($ids = "")
    {
        if ($ids) {
            $pk = $this->model->getPk();
            $adminIds = $this->getDataLimitAdminIds();
            if (is_array($adminIds)) {
                $this->model->where($this->dataLimitField, 'in', $adminIds);
            }
            Db::startTrans();

            $list = $this->model->where($pk, 'in', $ids)->select();

            if(!empty($list)){
                $this->deleteChild($ids);
            }
            $count = true;
            try {
//                foreach ($list as $k => $v) {
//                    $count += $v->delete();
//                }
                Db::commit();
            } catch (PDOException $e) {
                Db::rollback();
                $this->error($e->getMessage());
            } catch (\think\Exception $e) {
                Db::rollback();
                $this->error($e->getMessage());
            }
            if ($count) {
                $this->success();
            } else {
                $this->error(__('No rows were deleted'));
            }
        }
        $this->error(__('Parameter %s can not be empty', 'ids'));
    }

    public function deleteChild($ids = ''){
        if(empty($ids)){
            return false;
        }
        $list = db('area')->where('id', 'in', $ids)->delete();
        if(!empty($list)){
            $tempList = db('area')->where('pid', 'in', $ids)->column('id');
            if(!empty($tempList)){
                $this->deleteChild($tempList);
            }
        }
    }


}
