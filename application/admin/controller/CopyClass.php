<?php
namespace app\admin\controller;


use think\Controller;
use think\Db;
use think\Exception;
use think\exception\PDOException;

class CopyClass extends Controller {

    /**
     * 检查语言是否存在
     * @param int $beforeLanguage
     * @param int $afterLanguage
     * @return bool
     * @throws \think\db\exception\DataNotFoundException
     * @throws \think\db\exception\ModelNotFoundException
     * @throws \think\exception\DbException
     */
    public function existLanguage($beforeLanguage = 0 , $afterLanguage = 0){
        if(empty($beforeLanguage) || empty($afterLanguage)){
            return false;
        }

        if(!db('language')->where( 'language_id' , $beforeLanguage)->find()){
            return false;
        }

        if(!db('language')->where( 'language_id' , $afterLanguage)->find()){
            return false;
        }

        return true;
    }


    /**
     * 添加分类
     * @param $pid
     * @param array $child
     * @param string $tableName
     * @param int $afterLanguage
     * @return bool
     */
    public function insertChildType($pid , $child = [] , $tableName = '' , $afterLanguage){
        if(empty($child) || empty($tableName)){
            return true;
        }
        foreach($child as $v){
            $insetData = $v;
            $insetData['copy_id'] = $v['id'];
            $insetData['pid'] = $pid;
            $insetData['language_id'] = $afterLanguage;
            unset($insetData['id'] , $insetData['hasChild']);
            if(isset($insetData['hasChild'])){
                $thisChild = $insetData['hasChild'];
                unset($insetData['hasChild']);
                $topId = db($tableName)->insertGetId($insetData);
                $this->insertChildType($topId , $thisChild , $tableName);
            }else{
                $topId = db($tableName)->insertGetId($insetData);
            }
            unset($topId);
        }

        return true;
    }


    /***
     * 复制新闻模块
     * @return array
     * @throws \think\db\exception\DataNotFoundException
     * @throws \think\db\exception\ModelNotFoundException
     * @throws \think\exception\DbException
     */
    public function copyNews(){
        if($this->request->isPost()){
//            $data = input('post.');
            $data = $this->request->post("row/a");

            if(!isset($data['beforeLanguage']) || !isset($data['afterLanguage']) || empty($data['beforeLanguage']) || empty($data['afterLanguage'])){
                return ['code' => 0 , 'msg' => '没有选择语言' , 'data' =>$data];
            }

            $beforeLanguage = $data['beforeLanguage'];
            $afterLanguage = $data['afterLanguage'];

            $isCopyType = isset($data['isCopyType']) ? $data['isCopyType'] : 0;

            /**判断两种语言是否存在**/
            if(!$this->existLanguage( $beforeLanguage , $afterLanguage )){
                return ['code' => 0 , 'msg' => '选择语言不存在，请先配置'];
            }

            if($beforeLanguage == $afterLanguage){
                return ['code' => 0 , 'msg' => '复制的语言和被复制语言不能是一样'];
            }

            Db::startTrans();
            try {

                /**1、是否需要复制新闻分类**/
                if($isCopyType == 1){
                    $typeArray = db('news_category')->where(['language_id' => $beforeLanguage ])->select();
                    $treeArr = Tree($typeArray , 'id' , 'pid' , 'hasChild');
                    foreach ($treeArr as $k => $v){
                        $insetData = $v;
                        $insetData['copy_id'] = $v['id'];
                        $insetData['language_id'] = $afterLanguage;
                        unset($insetData['id'] , $insetData['hasChild']);
                        $topId = db('news_category')->insertGetId($insetData);
                        if(isset($v['hasChild'])){
                            $this->insertChildType($topId , $v['hasChild'] , 'news_category' , $afterLanguage);
                        }
                        unset($topId);
                    }
                    unset($typeArray , $treeArr);
                }

                /**2、复制新闻的基础信息表**/
                $beforeNewsData = db('news')->where(['language_id' => $beforeLanguage])->order('id' , 'desc')->select();
                $beforeNewsIdArray = db('news')->where(['language_id' => $beforeLanguage])->column('id');

                foreach ($beforeNewsData as $k => &$v){
                    $v['copy_id'] = $v['id'];
                    $v['language_id'] = $afterLanguage;
                    unset($v['id']);
                }

                db('news')->insertAll($beforeNewsData);

                if(!empty($beforeNewsIdArray)){
                    /**3、复制新闻的详情表**/
                    $beforeNewsDescriptionData = db('news_description')->whereIn('news_id' , $beforeNewsIdArray)->field('description_id' , true)->select();
                    foreach ($beforeNewsDescriptionData as &$v){
                        $v['copy_id'] = $v['news_id'];
                        $v['news_id'] = 0;
                    }
                    db('news_description')->insertAll($beforeNewsDescriptionData);
                    /**更新新闻详情表的news_id信息**/
                    $sql = 'UPDATE fa_news_description t1 INNER JOIN fa_news as t2 on t1.copy_id = t2.copy_id AND t2.language_id = '.$afterLanguage.' SET t1.news_id = t2.id where t1.copy_id > 0 AND t2.language_id = '.$afterLanguage;
                    Db::execute($sql);

                    /**更新新闻的分类id**/
                    if($isCopyType == 1){
                        $typeSql = 'UPDATE fa_news t1 INNER JOIN fa_news_category AS t2 ON t1.news_category_id = t2.copy_id AND t2.language_id = '.$afterLanguage.'  SET t1.news_category_id = t2.id WHERE t1.copy_id > 0 AND t1.language_id = '.$afterLanguage;
                        Db::execute($typeSql);
                    }
                }
                Db::commit();
            }catch (PDOException $e) {
                Db::rollback();
                return ['code' => 0 , 'msg' => $e->getMessage()];
            } catch (Exception $e) {
                Db::rollback();
                return ['code' => 0 , 'msg' => $e->getMessage()];
            }


        }

        return ['code' => 1 , 'msg' => '复制成功'];
    }


    /**
     * 复制产品摸快
     * @return array
     * @throws \think\db\exception\DataNotFoundException
     * @throws \think\db\exception\ModelNotFoundException
     * @throws \think\exception\DbException
     */
    public function copyProduct(){
        if($this->request->isPost()){
//            $data = input('post.');
            $data = $this->request->post("row/a");

            if(!isset($data['beforeLanguage']) || !isset($data['afterLanguage']) || empty($data['beforeLanguage']) || empty($data['afterLanguage'])){
                return ['code' => 0 , 'msg' => '没有选择语言' , 'data' =>$data];
            }

            $beforeLanguage = $data['beforeLanguage'];
            $afterLanguage = $data['afterLanguage'];

            $isCopyType = isset($data['isCopyType']) ? $data['isCopyType'] : 0;

            /**判断两种语言是否存在**/
            if(!$this->existLanguage( $beforeLanguage , $afterLanguage )){
                return ['code' => 0 , 'msg' => '选择语言不存在，请先配置'];
            }


            if($beforeLanguage == $afterLanguage){
                return ['code' => 0 , 'msg' => '复制的语言和被复制语言不能是一样'];
            }
            Db::startTrans();
            try {

                /**1、是否需要复制产品分类**/
                if($isCopyType == 1){
                    $typeArray = db('product_category')->where(['language_id' => $beforeLanguage ])->select();
                    $treeArr = Tree($typeArray , 'id' , 'pid' , 'hasChild');

                    foreach ($treeArr as $k => $v){
                        $insetData = $v;
                        $insetData['copy_id'] = $v['id'];
                        $insetData['language_id'] = $afterLanguage;
                        unset($insetData['id'] , $insetData['hasChild']);
                        $topId = db('product_category')->insertGetId($insetData);
                        if(isset($v['hasChild'])){
                            $this->insertChildType($topId , $v['hasChild'] , 'product_category' , $afterLanguage);
                        }
                        unset($topId);
                    }
                    unset($typeArray , $treeArr);


                    /**复制产品分类和颜色关联表数据**/
                    $categoryColorRelationData = db('category_color_relation')->where(['language_id' => $beforeLanguage ])->select();
                    if(!empty($categoryColorRelationData)){
                        foreach ($categoryColorRelationData as $k => &$v){
                            $v['language_id'] = $afterLanguage;
                            $v['copy_id'] = $v['product_category_id'];
                            $v['copy_key_id'] = $v['id'];
                            unset($v['id'] );
                        }
                        db('category_color_relation')->insertAll($categoryColorRelationData);

                        unset($categoryColorRelationData);

                        $categoryColorRelationSql = 'UPDATE fa_category_color_relation t1 INNER JOIN fa_product_category AS t2 ON t1.copy_id = t2.copy_id AND t2.language_id = '.$afterLanguage.' SET t1.product_category_id = t2.id WHERE t1.language_id = '.$afterLanguage;
                        Db::execute($categoryColorRelationSql);

                    }


                    /**复制产品分类和长度（重量）关联表**/
                    $categoryAttributeRelationData = db('category_attribute_relation')->where(['language_id' => $beforeLanguage ])->select();
                    if(!empty($categoryAttributeRelationData)){
                        foreach ($categoryAttributeRelationData as $k => &$v){
                            $v['language_id'] = $afterLanguage;
                            $v['copy_id'] = $v['product_category_id'];
                            unset($v['id']);
                        }

                        db('category_attribute_relation')->insertAll($categoryAttributeRelationData);
                        unset($categoryAttributeRelationData);

                        $categoryColorRelationSql = 'UPDATE fa_category_attribute_relation t1 INNER JOIN fa_product_category AS t2 ON t1.copy_id = t2.copy_id AND t2.language_id = '.$afterLanguage.' SET t1.product_category_id = t2.id WHERE t1.language_id = '.$afterLanguage;
                        Db::execute($categoryColorRelationSql);
                    }


                }


                /**2、复制产品的基础信息表**/
                $beforeProductData = db('product')->where(['language_id' => $beforeLanguage])->order('id' , 'desc')->select();
                $beforeProductIdArray = db('product')->where(['language_id' => $beforeLanguage])->column('id');

                foreach ($beforeProductData as $k => &$v){
                    $v['copy_id'] = $v['id'];
                    $v['language_id'] = $afterLanguage;
                    unset($v['id']);
                }

                db('product')->insertAll($beforeProductData);

                if(!empty($beforeProductIdArray)){
                    /**3、复制产品的详情表**/
                    $beforeNewsDescriptionData = db('product_description')->whereIn('product_id' , $beforeProductIdArray)->field('description_id' , true)->select();
                    foreach ($beforeNewsDescriptionData as &$v){
                        $v['copy_id'] = $v['product_id'];
                        $v['product_id'] = 0;
                    }
                    db('product_description')->insertAll($beforeNewsDescriptionData);
                    unset($beforeNewsDescriptionData);


                    /**更新产品详情表的product_id信息**/
                    $sql = 'UPDATE fa_product_description t1 INNER JOIN fa_product as t2 on t1.copy_id = t2.copy_id AND t2.language_id = '.$afterLanguage.' SET t1.product_id = t2.id where t1.copy_id > 0 AND t2.language_id = '.$afterLanguage;
                    Db::execute($sql);

                    if($isCopyType == 1){
                        /**更新产品的分类id**/
                        $typeSql = 'UPDATE fa_product t1 INNER JOIN fa_product_category AS t2 ON t1.product_category = t2.copy_id AND t2.language_id = '.$afterLanguage.'  SET t1.product_category = t2.id WHERE t1.copy_id > 0 AND t1.language_id = '.$afterLanguage;
                        Db::execute($typeSql);

                        /**更新重量属性**/
                        $weightSql = 'UPDATE fa_product t1 INNER JOIN fa_category_attribute_relation AS t2 ON t1.weight = t2.copy_id AND t2.language_id = '.$afterLanguage.'  SET t1.weight = t2.id WHERE t1.copy_id > 0 AND t1.language_id = '.$afterLanguage;

                        Db::execute($weightSql);


                        /**复制颜色关联表**/
                        $productColorRelationData = db('product_color_relation')->whereIn('product_id' , $beforeProductIdArray)->select();
                        foreach ($productColorRelationData as &$v){
                            $v['copy_id'] = $v['color_id'];
                            $v['copy_product_id'] = $v['product_id'];
                            $v['product_id'] = 0;
                            $v['color_id'] = 0;
                        }

                        db('product_color_relation')->insertAll($productColorRelationData);
                        unset($productColorRelationData);
                        $colorRelationSql = 'UPDATE fa_product_color_relation t1 INNER JOIN fa_product AS t2 ON t1.copy_product_id = t2.copy_id AND t2.language_id = '.$afterLanguage.'  INNER JOIN fa_category_color_relation AS t3 ON t1.copy_id = t3.copy_key_id AND t3.language_id = '.$afterLanguage.' SET t1.product_id = t2.id , t1.color_id = t3.id WHERE t1.copy_id > 0 AND t1.copy_product_id > 0 ';


                        Db::execute($colorRelationSql);

                        $initColorRelationSql = 'UPDATE fa_product_color_relation SET copy_id = 0,copy_product_id=0';
                        Db::execute($initColorRelationSql);
                    }
                }
                Db::commit();
            }catch (PDOException $e) {
                Db::rollback();
                return ['code' => 0 , 'msg' => $e->getMessage()];
            } catch (Exception $e) {
                Db::rollback();
                return ['code' => 0 , 'msg' => $e->getMessage()];
            }

        }

        return ['code' => 1 , 'msg' => '复制成功'];
    }




















}