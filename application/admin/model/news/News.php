<?php

namespace app\admin\model\news;

use think\Model;


class News extends Model
{



    const IS_EXTERNAL_TRUE = 1;//有外链
    const IS_EXTERNAL_FALSE = 2;//无外链

    const IS_OPEN_TRUE = 1;//可见
    const IS_OPEN_FALSE = 2;//不可见




    // 表名
    protected $name = 'news';
    
    // 自动写入时间戳字段
    protected $autoWriteTimestamp = 'int';

    // 定义时间戳字段名
    protected $createTime = 'createtime';
    protected $updateTime = 'updatetime';
    protected $deleteTime = false;

    // 追加属性
    protected $append = [
        'is_open_text',
        'is_external_text',
        'showtime_text'
    ];



    
    public function getIsOpenList()
    {
        return ['1' => __('Is_open 1'), '2' => __('Is_open 2')];
    }

    public function getIsExternalList()
    {
        return ['1' => __('Is_external 1'), '2' => __('Is_external 2')];
    }


    public function getIsOpenTextAttr($value, $data)
    {
        $value = $value ? $value : (isset($data['is_open']) ? $data['is_open'] : '');
        $list = $this->getIsOpenList();
        return isset($list[$value]) ? $list[$value] : '';
    }


    public function getIsExternalTextAttr($value, $data)
    {
        $value = $value ? $value : (isset($data['is_external']) ? $data['is_external'] : '');
        $list = $this->getIsExternalList();
        return isset($list[$value]) ? $list[$value] : '';
    }


    public function getShowtimeTextAttr($value, $data)
    {
        $value = $value ? $value : (isset($data['showtime']) ? $data['showtime'] : '');
        return is_numeric($value) ? date("Y-m-d H:i:s", $value) : $value;
    }

    protected function setShowtimeAttr($value)
    {
        return $value === '' ? null : ($value && !is_numeric($value) ? strtotime($value) : $value);
    }

    /**
     * 获取关联内容
     * @return \think\model\relation\HasOne
     */
    public function NewsDescription(){
        return $this->hasOne(NewsDescription::class , 'news_id' , 'id')->setEagerlyType(0);
    }


    /**
     * 获取分类
     * @return \think\model\relation\HasOne
     */
    public function Category(){
        return $this->hasOne(NewsCategory::class , 'id' , 'news_category_id');
    }


    /**
     * 获取关联内容
     * @return \think\model\relation\HasOne
     */
    public function Description(){
        return $this->hasOne(NewsDescription::class , 'news_id' , 'id');
    }


}
