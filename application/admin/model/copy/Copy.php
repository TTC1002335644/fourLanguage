<?php

namespace app\admin\model\copy;

use think\Model;


class Copy extends Model
{

    

    

    // 表名
    protected $name = 'copy';
    
    // 自动写入时间戳字段
    protected $autoWriteTimestamp = false;

    // 定义时间戳字段名
    protected $createTime = false;
    protected $updateTime = false;
    protected $deleteTime = false;

    // 追加属性
    protected $append = [
        'type_text',
        'is_copy_type_text'
    ];
    

    
    public function getTypeList()
    {
        return ['1' => __('Type 1'), '2' => __('Type 2')];
    }

    public function getIsCopyTypeList()
    {
        return ['0' => __('Is_copy_type 0'), '1' => __('Is_copy_type 1')];
    }


    public function getTypeTextAttr($value, $data)
    {
        $value = $value ? $value : (isset($data['type']) ? $data['type'] : '');
        $list = $this->getTypeList();
        return isset($list[$value]) ? $list[$value] : '';
    }


    public function getIsCopyTypeTextAttr($value, $data)
    {
        $value = $value ? $value : (isset($data['is_copy_type']) ? $data['is_copy_type'] : '');
        $list = $this->getIsCopyTypeList();
        return isset($list[$value]) ? $list[$value] : '';
    }




}
