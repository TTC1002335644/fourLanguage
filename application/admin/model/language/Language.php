<?php

namespace app\admin\model\language;

use think\Model;


class Language extends Model
{

    

    

    // 表名
    protected $name = 'language';
    
    // 自动写入时间戳字段
    protected $autoWriteTimestamp = false;

    // 定义时间戳字段名
    protected $createTime = false;
    protected $updateTime = false;
    protected $deleteTime = false;

    // 追加属性
    protected $append = [

    ];


    /**
     * 获取语言列表
     * @return false|\PDOStatement|string|\think\Collection
     * @throws \think\db\exception\DataNotFoundException
     * @throws \think\db\exception\ModelNotFoundException
     * @throws \think\exception\DbException
     */
    public function getLanguageList(){
        $res = $this->order('weigh asc')->select();
        return $res;
    }

    /**
     * 获取语言id(后端)
     * @param string $aliasName
     * @return int|mixed
     * @throws \think\db\exception\DataNotFoundException
     * @throws \think\db\exception\ModelNotFoundException
     * @throws \think\exception\DbException
     */
    public function getLanguageId($aliasName = ''){
        $model = $this->where(['aliasname' => $aliasName])->field(['language_id'])->find();
        if(empty($model)){
            $model = $this->where(['aliasname' => 'zh-cn'])->field(['language_id'])->find();
        }
        if(!empty($model)){
            return $model->language_id;
        }else{
            return 0;
        }
    }

    /**
     * 获取语言id(前端)
     * @param string $aliasName
     * @return array|int
     * @throws \think\db\exception\DataNotFoundException
     * @throws \think\db\exception\ModelNotFoundException
     * @throws \think\exception\DbException
     */
    public function getLanguageIdWeb($aliasName = ''){
        $model = $this->where(['aliasname' => $aliasName])->field(['language_id','aliasname','domain_name','language_name'])->find();
        if(empty($model)){
            $model = $this->where(['aliasname' => 'zh-cn'])->field(['language_id','aliasname','domain_name','language_name'])->find();
        }
        if(!empty($model)){
            return ['id' => $model->language_id , 'code' => $model->aliasname , 'domain_name' => $model->domain_name , 'language_name' => $model->language_name  ];
        }else{
            //根据域名来获取
            return ['id' => 0, 'code' => '' , 'domain_name' => '/' , 'language_name' => ''];
        }
    }



    public function getLanguageIdFront($aliasName = ''){

        $model = $this->where(['domain_name' => $_SERVER['HTTP_HOST']])->field(['language_id','aliasname','domain_name'])->find();

        if(empty($model)){
            $model = $this->where(['aliasname' => 'zh-cn'])->field(['language_id','aliasname','domain_name'])->find();
        }
        if(!empty($model)){
            return ['id' => $model->language_id , 'code' => $model->aliasname , 'domain_name' => $model->domain_name];
        }else{
            //根据域名来获取
            return ['id' => 0, 'code' => '' , 'domain_name' => '/'];
        }
    }












}
