<?php

namespace app\admin\model\download;

use think\Model;


class DownloadCategory extends Model
{

    

    

    // 表名
    protected $name = 'download_category';
    
    // 自动写入时间戳字段
    protected $autoWriteTimestamp = 'int';

    // 定义时间戳字段名
    protected $createTime = 'createtime';
    protected $updateTime = 'updatetime';
    protected $deleteTime = false;

    // 追加属性
    protected $append = [
        'is_open_text'
    ];

    const IS_OPEN_TRUE = 1;//可见
    const IS_OPEN_FALSE = 0;//不可见
    

    protected static function init()
    {
        self::afterInsert(function ($row) {
            $pk = $row->getPk();
            $row->getQuery()->where($pk, $row[$pk])->update(['weigh' => $row[$pk]]);
        });
    }

    
    public function getIsOpenList()
    {
        return ['0' => __('Is_open 0'), '1' => __('Is_open 1')];
    }


    public function getIsOpenTextAttr($value, $data)
    {
        $value = $value ? $value : (isset($data['is_open']) ? $data['is_open'] : '');
        $list = $this->getIsOpenList();
        return isset($list[$value]) ? $list[$value] : '';
    }




}
