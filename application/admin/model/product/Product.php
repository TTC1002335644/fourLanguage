<?php

namespace app\admin\model\product;

use think\Model;


class Product extends Model
{

    

    

    // 表名
    protected $name = 'product';
    
    // 自动写入时间戳字段
    protected $autoWriteTimestamp = 'int';

    // 定义时间戳字段名
    protected $createTime = 'createtime';
    protected $updateTime = 'updatetime';
    protected $deleteTime = false;

    // 追加属性
    protected $append = [
        'is_open_text',
        'added_time_text'
    ];

    const IS_OPEN_TRUE = 1;//可见
    const IS_OPEN_FALSE = 0;//不可见

    
    public function getIsOpenList()
    {
        return ['1' => __('Is_open 1'), '2' => __('Is_open 2')];
    }


    public function getIsOpenTextAttr($value, $data)
    {
        $value = $value ? $value : (isset($data['is_open']) ? $data['is_open'] : '');
        $list = $this->getIsOpenList();
        return isset($list[$value]) ? $list[$value] : '';
    }


    public function getAddedTimeTextAttr($value, $data)
    {
        $value = $value ? $value : (isset($data['added_time']) ? $data['added_time'] : '');
        return is_numeric($value) ? date("Y-m-d H:i:s", $value) : $value;
    }

    protected function setAddedTimeAttr($value)
    {
        return $value === '' ? null : ($value && !is_numeric($value) ? strtotime($value) : $value);
    }

    /**
     * 详情
     * @return \think\model\relation\HasOne
     */
    public function description(){
        return $this->hasOne(ProductDescription::class , 'product_id' , 'id');
    }

    /**
     * 分类
     * @return \think\model\relation\HasOne
     */
    public function category(){
        return $this->hasOne(ProductCategory::class , 'id' , 'product_category' );
    }

    /**
     * 颜色
     * @return \think\model\relation\BelongsTo
     */
    public function Color(){
        return $this->belongsTo(ProductColorRelation::class , 'id' , 'product_id' )->setEagerlyType(0);
//        return $this->hasOne(ProductColorRelation::class , 'id' , 'product_id' );
        return $this->hasOne(ProductColorRelation::class  , 'product_id', 'id' );
    }

    /**
     * 颜色
     * @return \think\model\relation\BelongsTo
     */
    public function Weights(){
        return $this->belongsTo(CategoryAttributeRelation::class , 'weight' , 'id' );
    }


    /**
     * 分类
     * @return \think\model\relation\HasOne
     */
    public function Productattr(){
        return $this->hasOne(ProductAttribute::class , 'id' , 'product_category');
    }






}
