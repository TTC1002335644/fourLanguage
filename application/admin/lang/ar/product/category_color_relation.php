<?php

return [
    'Id'                  => 'ID',
    'Language_id'         => '语言id',
    'Product_category_id' => '产品',
    'Color_name'          => '颜色名称',
    'Sort'                => '排序',
    'Createtime'          => '创建时间'
];
