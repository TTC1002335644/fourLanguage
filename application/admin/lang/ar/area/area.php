<?php

return [
    'Id'          => 'ID',
    'Language_id' => '语言ID',
    'Pid'         => '所属父级',
    'Name'        => '地区名字',
    'Image'        => '左侧图片',
    'Data_json'   => '数据',
    'Append'   => 'Add',
];
