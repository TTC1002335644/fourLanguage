<?php

return [
    'Id'      => 'ID',
    'Image'   => '背景',
    'Content' => '内容',
    'One_value' => '第一个值',
    'Two_value' => '第二个值',
    'Three_value' => '第三个值',
    'Four_value' => '第四个值',
];
