<?php

return [
    'Id'             => 'ID',
    'Type'           => '类型',
    'Type 1'         => '新闻复制',
    'Type 2'         => '产品复制',
    'Is_copy_type'   => '是否复制类型',
    'Is_copy_type 0' => '否',
    'Is_copy_type 1' => '是',
    'beforeLanguage' => '被复制语言',
    'afterLanguage' => '复制后的语言',
];
