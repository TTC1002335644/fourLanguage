<?php

return [
    'Album_id'          => '案例ID',
    'Language_id'       => '语言',
    'Album_category_id' => '所属分类',
    'Title'             => '案例标题',
    'Image'             => '封面',
    'Views'             => '浏览数',
    'Createtime'        => '上传时间',
    'Updatetime'        => '更新时间'
];
