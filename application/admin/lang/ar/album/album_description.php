<?php

return [
    'Description_id'  => '详情ID',
    'Album_id'        => '案例ID',
    'Content'         => '内容',
    'Seo_title'       => 'SEO标题',
    'Seo_keywork'     => 'SEO关键字',
    'Seo_description' => 'SEO描述'
];
