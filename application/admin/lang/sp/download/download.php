<?php

return [
    'Id'              => 'ID',
    'Language_id'     => '语言',
    'Name'            => '名字',
    'Category_id'     => '分类',
    'Image'           => '封面',
    'Author'          => '作者',
    'Content'         => '内容',
    'Seo_title'       => 'SEO标题',
    'Seo_keywork'     => 'SEO关键字',
    'Seo_description' => 'SEO详情',
    'Is_open'         => '状态',
    'Is_open 0'       => '禁用',
    'Is_open 1'       => '正常',
    'Createtime'      => '创建时间',
    'Updatetime'      => '更新时间',
    'Views'            => '阅读次数',
    'File'            => '文件',

];
