<?php

return [
    'Id'            => 'ID',
    'Logo'          => '网站Logo',
    'Favicon'       => '网站小图标',
    'Record_number' => '备案号',
    'Record_url'    => '备案链接',
    'Contact'       => '联系人',
    'Telphone'      => '手机',
    'Phone'         => '电话',
    'Mail'          => '邮箱',
    'Address'       => '地址',
    'example'       => '例子：京ICP备05087018号2',
    'Product introduction'       => '产品中心简介',
];
