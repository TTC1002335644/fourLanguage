<?php

return [
    'Banner_id'   => 'ID',
    'Language_id' => '语言',
    'Image'       => '图片',
    'Jump_url'    => '跳转链接',
    'Sort'        => '排序',
    'Remark'      => '备注',
    'Createtime'  => '上传时间',
    'Updatetime'  => '更新时间'
];
