<?php

return [
    'Id'          => 'ID',
    'Language_id' => '语言ID',
    'Pid'         => '父ID',
    'Name'        => '分类名称',
    'Image'       => '封面',
    'Weigh'       => '权重',
    'Introduction'       => '简介',
    'Is_open'     => '状态',
    'Is_open 0'   => '禁用',
    'Is_open 1'   => '正常',
    'Is_recommend'     => '推荐',
    'Is_recommend 0'   => '否',
    'Is_recommend 1'   => '是',
    'Attribute'   => '长度或重量',
    'Attribute exist'   => '已添加',
    'Color'   => '颜色',
    'Color exist'   => '已添加',
    'Attribute value'   => '属性值',
    'Tips'   => '一行一个属性值，按回车键换行',
    'Createtime'  => '创建时间',
    'Updatetime'  => '更新时间'
];
