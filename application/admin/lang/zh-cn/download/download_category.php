<?php

return [
    'Id'          => 'ID',
    'Language_id' => '语言id',
    'Name'        => '分类名',
    'Weigh'       => '权重',
    'Is_open'     => '状态',
    'Is_open 0'   => '禁用',
    'Is_open 1'   => '正常',
    'Createtime'  => '创建时间',
    'Updatetime'  => '更新时间'
];
