<?php

return [
    'Language_id'   => 'ID',
    'Aliasname'     => '语言别名',
    'Language_name' => '语言名',
    'Image' => '图片',
    'Domain_name' => '域名',
    'Domain_placeholder' => '输入域名不带http://或https://',
];
