<?php

return [
    'Id'         => 'ID',
    'Name'       => '名字',
    'Attribute'  => '属性',
    'Createtime' => '创建时间',
    'Updatetime' => '更新时间'
];
