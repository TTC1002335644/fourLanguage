<?php

return [
    'Product_id'       => '产品ID',
    'Language_id'      => '语言',
    'Product_category' => '产品分类',
    'Product_category first' => '产品一级分类',
    'Product_category second' => '产品二级分类',
    'Product_attribute_module' => '产品属性模板',
    'Cover_image' => '列表封面图',
    'Images' => '缩列图',
    'Title'            => '产品名',
    'Weight'            => '长度或重量',
    'Color'            => '颜色',
    'Min_weight'            => '最小重量',
    'Max_weight'            => '最大重量',
    'Attribute'        => '参数',
    'Hr'        => '抗病性',
    'File'        => '文件下载',
    'Views'            => '点击次数',
    'Is_open'          => '状态',
    'Is_open 1'        => '可见',
    'Is_open 2'        => '不可见',
    'Added_time'       => '上架时间',
    'Createtime'       => '创建时间',
    'Updatetime'       => '更新时间',
    'afterLanguage'            => '复制后的语言',

];
