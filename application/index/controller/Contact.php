<?php
namespace app\index\controller;
use think\Controller;

class Contact extends Base {

    public function index(int $language_id = 0){
        $data = db('contact')->where(['language_id' => $this->paramLanguageId])->find();
        $this->view->assign('data' , $data);
        return $this->view->fetch();
    }

}