<?php
namespace app\index\controller;

use app\admin\model\news\News as NewsModel;

class News extends Base{
    /**
     * 新闻列表
     * @param int $type
     * @param int $limit
     * @return string
     * @throws \think\Exception
     * @throws \think\exception\DbException
     */
    public function index($type = 0 , $limit = 9){
        //获取分类
        $categoryData = db('news_category')->where(['language_id' => $this->paramLanguageId])->order('weigh','asc')->order('createtime','desc')->page(1,4)->select();

        $option = ['language_id' => $this->paramLanguageId , 'is_open' => NewsMOdel::IS_OPEN_TRUE];
        if(!empty($type)){
            $option['news_category_id'] = $type;
        }

        $data = db('news')->where($option)->order('showtime' , 'desc')->paginate($limit , false , ['query' => $this->request->param()] );
        $this->view->assign('data' , $data);
        $this->view->assign('type' , $type);
        $this->view->assign('categoryData' , $categoryData);
        return $this->view->fetch();
    }

    /**
     * 获取新闻详情
     * @param int $id
     * @return array|false|\PDOStatement|string|\think\Model
     * @throws \think\db\exception\DataNotFoundException
     * @throws \think\db\exception\ModelNotFoundException
     * @throws \think\exception\DbException
     */
    public function detail(int $id = 0){
        $data = (new NewsModel())->where(['id' => $id , 'is_open' => NewsModel::IS_OPEN_TRUE , 'language_id' => $this->paramLanguageId])->with([
            'description',
            'Category'
        ])->find();
        if(empty($data)){
            throw new \think\exception\HttpException(404 , __('News not exist'));
        }
        (new NewsModel())->where(['id' => $id] )->setInc('views');
        $condition = [ 'language_id' => $this->paramLanguageId , 'is_open' => NewsModel::IS_OPEN_TRUE ];
        $field = ['id' , 'title' , 'showtime' ,'is_external' , 'external_url'];
        $prevData = (new NewsModel)->where($condition)->where('id' , '<' , $id)->order('id','desc')->field($field)->find();
        $nextData = (new NewsModel)->where($condition)->where('id' , '>' , $id)->order('id','asc')->field($field)->find();
        $this->view->assign('data' , $data);
        $this->view->assign('prev' , $prevData);
        $this->view->assign('next' , $nextData);
        return $this->view->fetch();
    }





    /**
     * 获取新闻列表
     * @param array $option
     * @param int $page
     * @return \think\Paginator
     * @throws \think\exception\DbException
     */
    public function getNewsList(array $option = [] , int $page = 10  ){
        $newsList = (new NewsModel())->where($option)->paginate($page);
        return $newsList;
    }




    /**
     * 获取上一张和下一张
     * @param int $news_id
     * @param int $language_id
     * @return array
     * @throws \think\db\exception\DataNotFoundException
     * @throws \think\db\exception\ModelNotFoundException
     * @throws \think\exception\DbException
     */
    public function getPrevNextNews(int $news_id = 0 , int $language_id = 1 , string $lang){
        $condition = [ 'language_id' => $language_id , 'is_open' => NewsModel::IS_OPEN_TRUE];
        $field = ['news_id' , 'title' , 'showtime' ,'is_external' , 'external_url'];
        $prevData = (new NewsModel)->where($condition)->where('news_id' , '<' , $news_id)->order('news_id','desc')->field($field)->find();
        $nextData = (new NewsModel)->where($condition)->where('news_id' , '>' , $news_id)->order('news_id','asc')->field($field)->find();
        if(!empty($prevData)){
            $prev = [
                'title' => $prevData->title,
                'url' => ($prevData->is_external == NewsModel::IS_OPEN_TRUE) ? $prevData->external_url :'./news_detail.html?lang='.$lang.'&news_id='.$prevData->news_id,
            ];
        }else{
            $prev = [
                'title' => __('None'),
                'url' => '#',
            ];
        }

        if(!empty($nextData)){
            $next = [
                'title' => $nextData->title,
                'url' => ($nextData->is_external == NewsModel::IS_OPEN_TRUE) ? $nextData->external_url : './news_detail.html?lang='.$lang.'&news_id='.$nextData->news_id,
            ];
        }else{
            $next = [
                'title' =>__('None'),
                'url' => '#',
            ];
        }
        return [
            'prev' => $prev,
            'next' => $next,
        ];
    }

}