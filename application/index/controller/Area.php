<?php


namespace app\index\controller;


class Area extends Base{

    /**
     * 列表
     * @return string
     * @throws \think\Exception
     */
    public function index(){
        //获取销售网络
        $area = db('area')->field(['id','pid','name'])->where(['language_id' => $this->paramLanguageId])->order('weigh','asc')->order('id','asc')->select();
        $jsArea = [];
        if(!empty($area)){
            $area = Tree($area , 'id');
            $jsArea = [];
//            foreach ($area as $v){
//                $jsArea[] = isset($v['hasChild']) ? $v['hasChild'] : [];
//            }
//
//            foreach ($jsArea as &$v){
//                foreach ($v as &$val){
//                    $val = empty($val['data_json']) ? [] : json_decode($val['data_json'] , true);
//                }
//            }
        }
        $this->view->assign('area' , $area);
//        dump($area);
        return $this->view->fetch();
    }

    /**
     * 详情
     * @param int $id
     * @return string
     * @throws \think\Exception
     * @throws \think\db\exception\DataNotFoundException
     * @throws \think\db\exception\ModelNotFoundException
     * @throws \think\exception\DbException
     */
    public function detail($id = 0){
        $data = [];
        if(!empty($id)){
            $data =  db('area')->where(['language_id' => $this->paramLanguageId , 'id' => $id])->find();
            if(!empty($data)){
                $data['data_json'] = empty($data['data_json']) ? [] : json_decode($data['data_json'] , true);

                if(!empty($data['data_json'])){
                    foreach($data['data_json'] as &$v){
                        $v['name'] = handleAttributesString($v['name'] , true , false);
                    }

                }
            }
        }
        $this->view->assign('data' , $data);
//        dump($data);
        return $this->view->fetch();
    }



}