<?php
namespace app\index\controller;

use app\admin\model\language\Language;
use app\common\controller\Frontend;

class Base extends Frontend{

    protected $noNeedLogin = '*';
    protected $noNeedRight = '*';
    protected $layout = '';

    /**
     * @throws \think\db\exception\DataNotFoundException
     * @throws \think\db\exception\ModelNotFoundException
     * @throws \think\exception\DbException
     */
    public function _initialize()
    {
        parent::_initialize();
        $languageList = (new Language())->getLanguageList();
        $menuList = (new WebMenu())->getMenuList($this->paramLanguageId);
        $webBasic = db('web_basic')->where('language_id' , $this->paramLanguageId)->find();
        //获取底部
        $footerConfig = db('footer')->where('language_id' , $this->paramLanguageId)->find();
//        dump($menuList);
        $this->view->assign('languageList' , $languageList);
        $this->view->assign('languageId' , $this->paramLanguageId);
        $this->view->assign('lang' , $this->paramLanguageCode);
        $this->view->assign('menuList' , $menuList);
        $this->view->assign('webBasic',$webBasic);
        $this->view->assign('footerConfig',$footerConfig);
    }


    public function getRouteInfo(){
        return [
            [
                'name' => url('news')
            ]
        ];
    }

}