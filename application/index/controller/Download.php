<?php


namespace app\index\controller;


use app\admin\model\download\Download as DownloadModel;
use app\admin\model\download\DownloadCategory as DownloadCategoryModel;

class Download extends Base{

    public function  index($type = 0 , $limit = 9){
//获取分类
        $categoryData = db('download_category')->where([
            'language_id' => $this->paramLanguageId,
            'is_open' => DownloadCategoryModel::IS_OPEN_TRUE
        ])->order('weigh','asc')->order('createtime','desc')->page(1,4)->select();

        $option = ['language_id' => $this->paramLanguageId , 'is_open' => DownloadModel::IS_OPEN_TRUE];
        if(!empty($type)){
            $option['category_id'] = $type;
        }

        $data = db('download')->where($option)->order('createtime','desc')->paginate($limit , false , ['query' => $this->request->param()] );
        $this->view->assign('data' , $data);
        $this->view->assign('type' , $type);
        $this->view->assign('categoryData' , $categoryData);
        return $this->view->fetch();
    }


    /**
     * 获取资料详情
     * @param int $id
     * @return array|false|\PDOStatement|string|\think\Model
     * @throws \think\db\exception\DataNotFoundException
     * @throws \think\db\exception\ModelNotFoundException
     * @throws \think\exception\DbException
     */
    public function detail(int $id = 0){
        $data = (new DownloadModel())
            ->where(['id' => $id , 'is_open' => DownloadModel::IS_OPEN_TRUE , 'language_id' => $this->paramLanguageId])
            ->with(['category'])
            ->find();
        if(empty($data)){
            throw new \think\exception\HttpException(404 , __('Content not exist'));
        }
        (new DownloadModel())->where(['id' => $id] )->setInc('views');
        $condition = [ 'language_id' => $this->paramLanguageId , 'is_open' => DownloadModel::IS_OPEN_TRUE ];
        $field = ['id' , 'name'  , 'image' , 'views'];
        $prevData = (new DownloadModel)->where($condition)->where('id' , '<' , $id)->order('id','desc')->field($field)->find();
        $nextData = (new DownloadModel)->where($condition)->where('id' , '>' , $id)->order('id','asc')->field($field)->find();
        $this->view->assign('data' , $data);
        $this->view->assign('prev' , $prevData);
        $this->view->assign('next' , $nextData);
        return $this->view->fetch();
    }



}