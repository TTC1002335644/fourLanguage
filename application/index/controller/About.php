<?php
namespace app\index\controller;

class About extends Base {

    /**
     * 关于我们
     * @return string
     * @throws \think\Exception
     * @throws \think\db\exception\DataNotFoundException
     * @throws \think\db\exception\ModelNotFoundException
     * @throws \think\exception\DbException
     */
    public function index(){
        db('about')->where(['language_id' => $this->paramLanguageId])->setInc('views');
        $data = db('about')->where(['language_id' => $this->paramLanguageId])->find();
        $this->view->assign('data' , $data);
        return $this->view->fetch();
    }

    public function getAbout(int $language_id = 0){
        $res = db('about')->where(['language_id' => $language_id])->find();
        return $res;
    }

}