<?php

namespace app\index\controller;

//use app\common\controller\Frontend;

use app\admin\model\news\News as NewsModel;
use app\admin\model\product\Product as ProductModel;
use app\admin\model\product\ProductCategory as ProductCategoryModel;
use app\admin\model\download\Download as DownloadModel;
use think\Request;

class Index extends Base {



    public function index(){
        $first = db('first')->where(['language_id' => $this->paramLanguageId])->find();
        $second = db('second')->where(['language_id' => $this->paramLanguageId])->find();
        $third = db('third')->where(['language_id' => $this->paramLanguageId])->find();
        $fourth = db('fourth')->where(['language_id' => $this->paramLanguageId])->find();
        $fifth = db('fifth')->where(['language_id' => $this->paramLanguageId])->find();
        $sixth = db('sixth')->where(['language_id' => $this->paramLanguageId])->find();

        if(!empty($third)){
        }
//        $thirdProduct = db('product')->whereIn('is_open',ProductModel::IS_OPEN_TRUE)->order('id' ,'desc')->limit(50)->select();
        $thirdProduct = db('product_category')
            ->where(['language_id' => $this->paramLanguageId])
            ->where(['is_open' => ProductCategoryModel::IS_OPEN_TRUE , 'pid' => 0])
            ->order('weigh' ,'asc')
            ->limit(50)
            ->select();


//        if(!empty($fifth)){
//            $fifthDownload = db('download')->field(['id' , 'name' , 'image'])->whereIn('id',$fifth['downloads'])->select();
//        }


        $fifthPage = Request::instance()->isMobile() ? 6 : 8;

        $fifthDownload = db('download')->field(['id' , 'name' , 'image'])
            ->where('is_open',DownloadModel::IS_OPEN_TRUE)
            ->where(['language_id' => $this->paramLanguageId])
            ->order('createtime' ,'desc')->limit($fifthPage)->select();
        //获取Banner
        $bannerList = (new Banner())->getBannerList($this->paramLanguageId);

        //获取销售网络
        $area = db('area')->where(['language_id' => $this->paramLanguageId])->order('weigh','asc')->order('id','asc')->select();
        $jsArea = [];
        if(!empty($area)){
            $area = Tree($area , 'id');
            $jsArea = [];
            foreach ($area as $v){
                $jsArea[] = isset($v['hasChild']) ? $v['hasChild'] : [];
            }

            foreach ($jsArea as &$v){
                foreach ($v as &$val){
                    $val = empty($val['data_json']) ? [] : json_decode($val['data_json'] , true);
                }
            }

        }


        $this->view->assign('bannerList' , $bannerList);
        $this->view->assign('area' , $area);
        $this->view->assign('jsArea' , $jsArea);

        $this->view->assign('first' , $first);
        $this->view->assign('second' , $second);
        $this->view->assign('third' , $third);
        $this->view->assign('fourth' , $fourth);
        $this->view->assign('fifth' , $fifth);
        $this->view->assign('sixth' , $sixth);


        $this->view->assign('thirdProduct' , $thirdProduct);
        $this->view->assign('fifthDownload' , $fifthDownload);
        return $this->view->fetch();
    }

    /**
     * 新闻版面
     * @param int $pageSize
     * @param int $pageNow
     * @return string
     * @throws \think\Exception
     * @throws \think\exception\DbException
     */
    public function article(int $page = 10 ){
        $condition = [
            'language_id' => $this->paramLanguageId,
            'is_open' => NewsModel::IS_OPEN_TRUE
        ];
        //获取新闻
        $newsList = (new News())->getNewsList($condition , $page);

        //获取Banner
        $bannerList = (new Banner())->getBannerList($this->paramLanguageId);

        $this->view->assign('bannerList' , $bannerList);
        $this->view->assign('newsList' , $newsList);
        return $this->view->fetch();
    }

    /**
     * 新闻详情
     * @param int $news_id
     * @return string
     * @throws \think\Exception
     * @throws \think\db\exception\DataNotFoundException
     * @throws \think\db\exception\ModelNotFoundException
     * @throws \think\exception\DbException
     */
    public function article_detail(int $news_id = 0)
    {
        $newsDetail =  (new News())->getNewsDetail($news_id);

        /**外链直接跳转**/
        if(!empty($newsDetail) && $newsDetail->is_external == NewsModel::IS_EXTERNAL_TRUE){
            header('Location: '.$newsDetail->external_url);
            exit();
        }

        /**没有这篇文章的时候，跳转到新闻列表**/
        if(empty($newsDetail)){
            $url = url('/news',['lang' => $this->paramLanguageCode]);
            header('Location: '.$url);
            exit();
        }

        /**获取上下篇**/
        $prveNext = (new News())->getPrevNextNews($news_id , $this->paramLanguageId , $this->paramLanguageCode);


        //获取Banner
        $bannerList = (new Banner())->getBannerList($this->paramLanguageId);
        $this->view->assign('bannerList' , $bannerList);

        $this->view->assign('newsDetail' , $newsDetail);
        $this->view->assign('prveNext' , $prveNext);
        return $this->view->fetch();
    }


    /**
     * 关于我们（公司简介）
     * @return string
     * @throws \think\Exception
     */
    public function about(){
        $aboutRes = (new About())->getAbout($this->paramLanguageId);

        //获取Banner
        $bannerList = (new Banner())->getBannerList($this->paramLanguageId);
        $this->view->assign('bannerList' , $bannerList);
        $this->view->assign('about' , $aboutRes);
        return $this->view->fetch();
    }

    /**
     * 成功案例
     * @return string
     * @throws \think\Exception
     */
    public function album(){
        //获取Banner
        $bannerList = (new Banner())->getBannerList($this->paramLanguageId);
        $this->view->assign('bannerList' , $bannerList);
        return $this->view->fetch();
    }

    /**
     * 案例详情
     * @return string
     * @throws \think\Exception
     */
    public function album_detail(){
        //获取Banner
        $bannerList = (new Banner())->getBannerList($this->paramLanguageId);
        $this->view->assign('bannerList' , $bannerList);
        return $this->view->fetch();
    }

    /**
     * 联系我们
     * @return string
     * @throws \think\Exception
     */
    public function contact(){
        $contact = (new Contact())->getDetail($this->paramLanguageId);
        //获取Banner
        $bannerList = (new Banner())->getBannerList($this->paramLanguageId);
        $this->view->assign('bannerList' , $bannerList);
        $this->view->assign('contact' , $contact);
        return $this->view->fetch();
    }

    /**
     * 产品页面
     * @return string
     * @throws \think\Exception
     * @throws \think\db\exception\DataNotFoundException
     * @throws \think\db\exception\ModelNotFoundException
     * @throws \think\exception\DbException
     */
    public function product(int $product_category = 0){
        //获取分类
        $productCategory = (new ProductCategory())->getProductCategory($this->paramLanguageId);
        if(!empty($productCategory)){
            foreach ($productCategory as $k => &$v){
                $v['url'] = url('/product' , ['lang' => $this->paramLanguageCode , 'product_category' => $v['id']]);
                $v['active'] = ($product_category == $v['id']) ? true : false;
            }
        }

        //获取Banner
        $bannerList = (new Banner())->getBannerList($this->paramLanguageId);
        $this->view->assign('productCategory' , $productCategory);
        $this->view->assign('bannerList' , $bannerList);
        return $this->view->fetch();
    }

    public function product_detail(){
        //获取Banner
        $bannerList = (new Banner())->getBannerList($this->paramLanguageId);
        $this->view->assign('bannerList' , $bannerList);
        return $this->view->fetch();
    }


    /**
     * 基地列表
     * @return string
     * @throws \think\Exception
     */
    public function bases(){
        //获取Banner
        $bannerList = (new Banner())->getBannerList($this->paramLanguageId);
        $this->view->assign('bannerList' , $bannerList);
        return $this->view->fetch();
    }

    /**
     * 基地详情
     * @return string
     * @throws \think\Exception
     */
    public function bases_detail(){
        //获取Banner
        $bannerList = (new Banner())->getBannerList($this->paramLanguageId);
        $this->view->assign('bannerList' , $bannerList);
        return $this->view->fetch();
    }

    /**
     * 在线留言
     * @return string
     * @throws \think\Exception
     */
    public function comment(){
        //获取Banner
        $bannerList = (new Banner())->getBannerList($this->paramLanguageId);
        $this->view->assign('bannerList' , $bannerList);
        return $this->view->fetch();
    }

}
