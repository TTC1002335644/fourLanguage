<?php
namespace app\index\controller;

use think\Controller;
use app\admin\model\product\ProductCategory as ProductCategoryModel;

class ProductCategory extends Controller{

    /**
     * 获取分类列表
     * @param int $language_id
     * @param int $limit
     * @return false|\PDOStatement|string|\think\Collection
     * @throws \think\db\exception\DataNotFoundException
     * @throws \think\db\exception\ModelNotFoundException
     * @throws \think\exception\DbException
     */
    public function getProductCategory(int $language_id = 0 , int $limit = 20){
        $option = ['language_id' => $language_id , 'status' => ProductCategoryModel::STATUS_ON];
        $res = db('product_category')->where($option)->limit(10)->select();
        return $res;
    }


}