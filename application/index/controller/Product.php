<?php
namespace app\index\controller;
use app\admin\model\product\ProductCategory as ProductCategoryModel;
use app\admin\model\product\Product as ProductModel;


class Product extends Base
{

    /**
     * 获取顶级分类id
     * @param int $pid
     * @return int
     */
    public function getTopParent($pid = 0){
        if(empty($pid)){
            return $pid;
        }
        $res = db('product_category')->where(['id' => $pid])->field(['pid' , 'id'])->find();
        if(!empty($res)){
            if(empty($res['pid'])){
                return $res['id'];
            }else{
                return $this->getTopParent($res['pid']);
            }
        }else{
            return 0;
        }
    }

    /**
     * 分类列表
     * @return string
     * @throws \think\Exception
     */
    public function category($pid = 0){
        $option = ['language_id' => $this->paramLanguageId,'is_open' =>ProductCategoryModel::IS_OPEN_TRUE];
        $current = db('product_category')->where($option)->where('id' , $pid)->find();
        $data = db('product_category')
            ->where($option)
            ->where('pid' , $pid)
            ->order('weigh','asc')
            ->order('createtime' , 'desc')
            ->select();
        $this->view->assign('data' , $data);
        $this->view->assign('current' , $current);
        $this->view->assign('pid' , $pid);
        return $this->view->fetch();
    }

    /**
     * 产品列表
     * @param int $type
     * @param int $productAttribute
     * @param int $productColor
     * @param int $parentType
     * @return string
     * @throws \think\Exception
     * @throws \think\db\exception\DataNotFoundException
     * @throws \think\db\exception\ModelNotFoundException
     * @throws \think\exception\DbException
     */
    public function index($type = 0 , $productAttribute = 0 , $productColor = 0 , int $parentType = 0){
        /***获取当前的分类*/
        $originType = $type;
        if(empty($type)){
            $type = $parentType;
        }

        $category = db('product_category')->where(['language_id' => $this->paramLanguageId ,'id' => $type ,'is_open' => ProductCategoryModel::IS_OPEN_TRUE])->find();

        if(empty($category)){
            throw new \think\exception\HttpException(404 , __('Category not exist'));
        }


        //同级分类
        $childCategory = db('product_category')->where([
            'language_id' => $this->paramLanguageId ,
//            'pid' => (!empty($parentType)) ? $parentType : $type,
            'pid' =>$category['pid'],
            'is_open' => ProductCategoryModel::IS_OPEN_TRUE
        ])->order('weigh' , 'asc')->select();


        $option = ['language_id' => $this->paramLanguageId ,'is_open' =>ProductModel::IS_OPEN_TRUE];
//        if(($type != $parentType && empty($parentType)) || ($type == $parentType && empty($originType))){
//            $productCategoryOption = array_merge(array_column($childCategory , 'id') , [$type]);
//        }else{
//            $productCategoryOption = [$type];
//        }

//        if(empty($type)){
//        }
        $option['product_category'] = $type;

        if(!empty($productAttribute)){
            $option['weight'] = $productAttribute;
        }
        $productIds = [];
        if(!empty($productColor)){
            $productIds = db('product_color_relation')->where('color_id', $productColor)->column('product_id');
            if(!empty($productIds)){
                $productIds = array_unique($productIds);
            }
        }

        /***产品数据*/
        $data = db('product')
            ->where($option)
//            ->whereIn('product_category' , $productCategoryOption)
//            ->whereIn('product_category' , $type)
            ->where(function($query)use($productIds , $productColor){
                if(!empty($productColor)){
                    $query->whereIn('id' , $productIds);
                }
            })
            ->order('added_time' , 'desc')
            ->select();

        /**分类下颜色和重量**/
//        $categoryOption = ['language_id' => $this->paramLanguageId , 'product_category_id' => $type ];
        $categoryOption = ['language_id' => $this->paramLanguageId , 'product_category_id' => $category['pid'] ];
//        if(!empty($parentType)){
//            $categoryOption['product_category_id'] = $parentType;
//        }

        $color = db('category_color_relation')->where($categoryOption)->order('weigh' , 'asc')->order('createtime' , 'desc')->select();

        $attribute = db('category_attribute_relation')->where($categoryOption)->order('weigh' , 'asc')->order('id' , 'asc')->select();
        $this->view->assign('data' , $data);
        $this->view->assign('type' , $type);
        $this->view->assign('pid' , $category['pid']);
        $this->view->assign('category' , $category);
        $this->view->assign('childCategory' , $childCategory);
        $this->view->assign('color' , $color);
        $this->view->assign('productColor' , $productColor);
        $this->view->assign('productAttribute' , $productAttribute);
        $this->view->assign('selectType' , $type);
        $this->view->assign('attribute' , $attribute);
        return $this->view->fetch();
    }


    /**
     * 产品详情
     * @param int $id
     * @return string
     * @throws \think\Exception
     * @throws \think\db\exception\DataNotFoundException
     * @throws \think\db\exception\ModelNotFoundException
     * @throws \think\exception\DbException
     */
    public function detail($id = 0){
        $baseOption = ['language_id' => $this->paramLanguageId ,'is_open' =>ProductModel::IS_OPEN_TRUE ];
        $option = ['language_id' => $this->paramLanguageId ,'is_open' =>ProductModel::IS_OPEN_TRUE , 'id' => $id];
        $data = db('product')
            ->where($option)
            ->find();
        if(empty($data)){
            throw new \think\exception\HttpException(404 , __('Product not exist'));
        }
        db('product') ->where($option)->setInc('views');

        $data['attribute'] = handleAttributesString( $data['attribute'] ,true , false);
        if(!empty($data['images'])){
            $data['images'] = explode(',' , $data['images']);
        }

        $prev = db('product')->where($baseOption)->where('id' , '<' , $id)->order('id' , 'desc') ->find();
        $next = db('product')->where($baseOption)->where('id' , '>' , $id)->order('id' , 'asc') ->find();

        $this->view->assign('data' , $data);
        $this->view->assign('prev' , $prev);
        $this->view->assign('next' , $next);
        $this->view->assign('isMobile' , request()->isMobile());
        return $this->view->fetch();
    }

}