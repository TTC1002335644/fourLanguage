<?php
namespace app\index\controller;

use think\Controller;
use app\admin\model\home\Banner as BannerModel;

class Banner extends Controller{

    /**
     * 获取banner列表
     * @param int $language_id
     * @return mixed
     */
    public function getBannerList(int $language_id = 0){
        $res = db('banner')->where(['language_id' => $language_id])->order('sort' , 'asc')->select();
        return $res;
    }

}