<?php
return [
    'Home' => 'Home',
    'About us' => 'About us',
    'Contact' => 'Contact us',
    'Current position' => 'Current position',
    'Product center' => 'Product center',
    'News center' => 'News center',
    'Download center' => 'Download center',
    'SALES NETWORK' => 'SALES NETWORK',
    'Createtime' => 'Release time',
    'Views' => 'Views',
    'Prev news' => 'Previous',
    'Next news' => 'Next',
    'Prev product' => 'Previous',
    'Next product' => 'Next',
    'News not exist' => 'News does not exist',
    'Product not exist' => 'Product does not exist',
    'Content not exist' => 'Content does not exist',
    'Category not exist' => 'Category does not exist',
    'Category search' => 'Category search',
    'Category type' => 'Category type',
    'Select category type' => 'Choose Fruit Properties',
    'Select product attribute' => 'Please select a variety type',
    'Select product color' => 'Choose fruit color',
    'Product attribute' => 'Product attribute',
    'Product color' => 'Product color',

    'Hr' => 'Disease resistance',
    'Download' => 'Download',

    'Search' => 'Search',
    'Reset' => 'Reset',

    'About company' => 'About us',

    'Year text' => 'Established in 2007',
    'Money text' => 'Research',
    'Company text' => 'Produce',
    'Invest text' => 'Sales',


    'Select language' => 'Select language',

    'Product center introduction' => '&nbsp;&nbsp;&nbsp;&nbsp;Hivester has successfully developed and promoted more than 100 excellent varieties such as tomatoes, eggplants, peppers, cucumbers, spinach, etc. throughout the country. Through strict test and demonstration system and systematic product chain management, it has continuously introduced new products suitable for cultivation in different regions. Excellent variety. In addition, the company also has a service refining and first-class service team, providing growers with the whole process of services from planting to harvesting all year round to maximize the farmers\' income.',

    'Contact person' => 'Contact person',
    'Phone' => 'Phone',
    'Mailbox' => 'Mailbox',
    'WeChat' => 'WeChat',
];
