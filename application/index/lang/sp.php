<?php
return [
    'Home' => 'Inicio',
    'About us' => 'Acerca de nosotros',
    'Contact' => 'Contáctanos',
    'Current position' => 'Posición actual',
    'Product center' => 'Centro de producto',
    'News center' => 'Centro de noticias',
    'Download center' => 'Centro de datos',
    'SALES NETWORK' => 'RED DE VENTAS',

    'Createtime' => 'Tiempo de liberación',
    'Views' => 'Vistas',
    'Prev news' => 'Anterior',
    'Next news' => 'Siguiente',
    'Prev product' => 'Anterior',
    'Next product' => 'Siguiente',
    'News not exist' => 'Las noticias no existen',
    'Product not exist' => 'El producto no existe.',
    'Content not exist' => 'El contenido no existe.',
    'Category not exist' => 'La clasificación no existe.',
    'Category search' => 'Búsqueda de variedades',
    'Category type' => 'Tipo de variedad',
    'Select category type' => 'Elija las propiedades de la fruta',
    'Select product attribute' => 'Por favor seleccione un tipo de variedad',
    'Select product color' => 'Elige color de fruta',
    'Product attribute' => 'Propiedades de la fruta',
    'Product color' => 'Color de la fruta',

    'Hr' => 'Resistencia a las enfermedades',
    'Download' => 'Descargar',

    'Search' => 'Buscar',
    'Reset' => 'Restablecer',

    'About company' => 'Acerca de nosotros',

    'Year text' => 'Establecido en 2007',
    'Money text' => 'Investigación',
    'Company text' => 'Producción',
    'Invest text' => 'Ventas',


    'Select language' => 'Elegir idioma',

    'Product center introduction' => '&nbsp;&nbsp;&nbsp;&nbsp;Hivester ha desarrollado y promovido con éxito más de 100 variedades excelentes, como tomates, berenjenas, pimientos, pepinos, espinacas, etc. en todo el país. Mediante un estricto sistema de demostración de pruebas y una gestión sistemática de la cadena de productos, ha introducido continuamente nuevos productos adecuados para el cultivo en diferentes regiones Excelente variedad Además, la compañía también cuenta con un equipo de servicio de refinación y servicio de primera clase, que brinda a los productores todo el proceso de servicios desde la siembra hasta la cosecha durante todo el año para maximizar los ingresos de los agricultores.',

    'Contact person' => 'Persona de contacto',
    'Phone' => 'Teléfono',
    'Mailbox' => 'Buzón',
    'WeChat' => 'WeChat',
];
