<?php
return [
    'Home' => '首页',
    'About us' => '关于我们',
    'Contact' => '联系我们',
    'Current position' => '当前位置',
    'Product center' => '产品中心',
    'News center' => '新闻中心',
    'Download center' => '资料中心',
    'SALES NETWORK' => '销售网络',
    'Createtime' => '发布时间',
    'Views' => '阅读次数',
    'Prev news' => '上一篇',
    'Next news' => '下一篇',
    'Prev product' => '上一个',
    'Next product' => '下一下',
    'News not exist' => '新闻不存在',
    'Product not exist' => '产品不存在',
    'Content not exist' => '内容不存在',
    'Category not exist' => '分类不存在',
    'Category search' => '产品搜索',
    'Category type' => '品种类型',
    'Select category type' => '选择果实属性',
    'Select product attribute' => '请选择品种类型',
    'Select product color' => '选择果实颜色',
    'Product attribute' => '果实属性',
    'Product color' => '果实颜色',

    'Hr' => '抗病性',
    'Download' => '下载',

    'Search' => '搜索',
    'Reset' => '重置',

    'About company' => '关于我们',

    'Year text' => '2007年成立',
    'Money text' => '科研',
    'Company text' => '生产',
    'Invest text' => '销售',


    'Select language' => '选择语言',

    'Product center introduction' => '&nbsp;&nbsp;&nbsp;&nbsp;海伟斯特在全国范围内成功开发与推广番茄、茄子、椒类、黄瓜、菠菜等优良品种百余个，通过严格的试验示范体系及系统的产品链管理，不断推出适合不同地区栽培的新优品种。另外，公司还拥有一支业务精炼、技术一流的服务队伍，常年为种植户提供从种植到收获的全过程服务，最大限度地提高种植户的收益。',

    'Contact person' => '联系人',
    'Phone' => '电话',
    'Mailbox' => '邮箱',
    'WeChat' => '微信',
];
