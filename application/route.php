<?php

// +----------------------------------------------------------------------
// | ThinkPHP [ WE CAN DO IT JUST THINK ]
// +----------------------------------------------------------------------
// | Copyright (c) 2006~2016 http://thinkphp.cn All rights reserved.
// +----------------------------------------------------------------------
// | Licensed ( http://www.apache.org/licenses/LICENSE-2.0 )
// +----------------------------------------------------------------------
// | Author: liu21st <liu21st@gmail.com>
// +----------------------------------------------------------------------
use \think\Route;

Route::get('index','index/index/index');//首页
Route::get('about','index/about/index');//关于我们
Route::get('news','index/news/index');//新闻列表
Route::get('newsDetail','index/news/detail');//新闻详情
Route::get('contact','index/contact/index');//联系我们
Route::get('download','index/download/index');//资料中心
Route::get('downloadDetail','index/download/detail');//资料详情

Route::get('product','index/product/category');//产品分类列表
Route::get('productList','index/product/index');//产品列表
Route::get('productDetail','index/product/detail');//产品详情



Route::get('areaIndex','index/area/index');//销售网络列表
Route::get('areaDetail','index/area/detail');//销售网络详情




Route::get('login','/admin/index/login');//登录


Route::post('test/news','admin/Copy/copyNews');
Route::post('test/product','admin/Copy/copyProduct');


return [
    //别名配置,别名只能是映射到控制器且访问时必须加上请求的方法
    '__alias__'   => [
    ],
    //变量规则
    '__pattern__' => [
    ],
//        域名绑定到模块
//        '__domain__'  => [
//            'admin' => 'admin',
//            'api'   => 'api',
//        ],
];
