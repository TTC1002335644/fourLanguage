define(['jquery', 'bootstrap', 'backend', 'table', 'form'], function ($, undefined, Backend, Table, Form) {

    var Controller = {
        index: function () {
            // 初始化表格参数配置
            Table.api.init({
                extend: {
                    index_url: 'language/language/index' + location.search,
                    add_url: 'language/language/add',
                    edit_url: 'language/language/edit',
                    del_url: 'language/language/del',
                    multi_url: 'language/language/multi',
                    table: 'language',
                }
            });

            var table = $("#table");

            // 初始化表格
            table.bootstrapTable({
                url: $.fn.bootstrapTable.defaults.extend.index_url,
                pk: 'language_id',
                sortName: 'language_id',
                columns: [
                    [
                        {checkbox: true},
                        {field: 'language_id', title: __('Language_id')},
                        {field: 'aliasname', title: __('Aliasname')},
                        {field: 'language_name', title: __('Language_name')},
                        {field: 'image', title: __('Image'),formatter: Table.api.formatter.image},
                        {field: 'domain_name', title: __('Domain_name'),formatter: Table.api.formatter.url},
                        {field: 'operate', title: __('Operate'), table: table, events: Table.api.events.operate, formatter: Table.api.formatter.operate}
                    ]
                ]
            });

            // 为表格绑定事件
            Table.api.bindevent(table);
        },
        add: function () {
            Controller.api.bindevent();
        },
        edit: function () {
            Controller.api.bindevent();
        },
        api: {
            bindevent: function () {
                Form.api.bindevent($("form[role=form]"));
            }
        }
    };
    return Controller;
});