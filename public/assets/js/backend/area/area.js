define(['jquery', 'bootstrap', 'backend', 'table', 'form'], function ($, undefined, Backend, Table, Form) {

    var Controller = {
        index: function () {
            // 初始化表格参数配置
            Table.api.init({
                extend: {
                    index_url: 'area/area/index' + location.search,
                    add_url: 'area/area/add',
                    edit_url: 'area/area/edit',
                    del_url: 'area/area/del',
                    multi_url: 'area/area/multi',
                    table: 'area',
                }
            });

            var table = $("#table");
            $(".btn-add").data("area", ["100%", "100%"]);
            $(".btn-edit").data("area", ["100%", "100%"]);
            table.on('post-body.bs.table', function (e, settings, json, xhr) {
                $(".btn-editone").data("area", ["100%", "100%"]);
            });

            // 初始化表格
            table.bootstrapTable({
                url: $.fn.bootstrapTable.defaults.extend.index_url,
                pk: 'id',
                sortName: 'id',
                escape:false,
                pagination: false,
                columns: [
                    [
                        {checkbox: true},
                        {field: 'id', title: __('Id')},
                        // {field: 'pid', title: __('Pid')},
                        {field: 'name', title: __('Name'),align:'left'},
                        {field: 'operate', title: __('Operate'), table: table, events: Table.api.events.operate, formatter: Table.api.formatter.operate}
                    ]
                ]
            });

            // 为表格绑定事件
            Table.api.bindevent(table);
        },
        add: function () {
            Controller.api.bindevent();
        },
        edit: function () {
            Controller.api.bindevent();
        },
        api: {
            bindevent: function () {
                $('.btn-append-one').click(function(){
                    var tempStr = `<dl>
                    <dd class="form-inline" style="margin-top:10px;">
                    <input type="text" class="form-control" value="联系人" size="10" readonly>
                    <input type="text" name="row[data_json][name][]" class="form-control langAr area-value" value=""> 
                    <span class="btn btn-sm btn-danger btn-remove-one"><i class="fa fa-times"></i></span>
                    </dd>
                    <dd class="form-inline">
                    <input type="text"  class="form-control" value="电话" size="10" readonly>
                    <input type="text" name="row[data_json][phone][]" class="form-control langAr area-value" value=""> 
                    </dd>
                    <dd class="form-inline">
                    <input type="text" class="form-control" value="邮箱" size="10" readonly>
                    <input type="text" name="row[data_json][mail][]" class="form-control langAr area-value" value=""> 
                    </dd>
                    <dd class="form-inline">
                    <input type="text" class="form-control" value="微信" size="10" readonly>
                    <input type="text" name="row[data_json][wechat][]" class="form-control langAr area-value" value=""> 
                    </dd>
                    </dl>`;//没有用
                    var str = `
                    <dl>
                    <dd class="form-inline" style="margin-top:10px;">
<!--                    <input type="text" class="form-control" value="内容" size="10" readonly>-->
                    <textarea class="form-control  langAr area-value" rows="5" name="row[data_json][name][]" cols="50" style="min-height: 250px;"></textarea>
                    <span class="btn btn-sm btn-danger btn-remove-one"><i class="fa fa-times"></i></span>

                    </dd>
                    </dl>`;
                    $('#th-colums').before(str)
                });

                $(document).on('click' , '.btn-remove-one' ,function(){
                    $(this).parent().parent().remove()
                });


                Form.api.bindevent($("form[role=form]"));
            }
        }
    };
    return Controller;
});