define(['plupload'],function(Up){

    function getFormData() {
        return getChild($('#form-menu'));
    }

    function getChild(obj) {
        var groupObj = obj.children('.group');
        var returnData = [];
        $.each(groupObj, function (k, v) {
            let name = $(v).children('.group-row').children('.menu-name').val();
            let url = $(v).children('.group-row').children('.menu-url').val();
            if (name.length > 0) {
                returnData.push({
                    name: name,
                    url: url,
                    child: getChild($(v))
                });
            }
        });
        return returnData;
    }

    function saveData(url){
        $('.btn-save').click(function () {
            var data = getFormData();
            var lang = $('.language').val();
            $.ajax({
                url: url,
                data: {data: JSON.stringify(data), 'lang': lang},
                type: 'post',
                dataType: 'json',
                success: function (res) {
                    layer.alert(res.msg, {
                        icon: res.code,
                        skin: 'layer-ext-moon'
                    },function(){
                        location.reload();
                    });
                },
                error: function (error) {
                    console.log(error);
                }
            });
        });
    }


    return saveData;

});