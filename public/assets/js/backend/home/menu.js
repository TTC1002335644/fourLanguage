define(['plupload'],function(Up){

	var apiUrl = {};
    var strMap = {};


        /* 展示事件 */
	$(document).on('click' , '.group-icon' ,function(){
		let status = $(this).attr('data-status');
		if(status == 'off'){
			$(this).css({
				'transform' : 'rotate(90deg)'
			}).attr('data-status','on');
			$(this).parent().siblings('.group').finish().show('fast','linear');
		}else{
			$(this).css({
				'transform' : 'rotate(0deg)'
			}).attr('data-status','off');
			$(this).parent().siblings('.group').finish().hide('fast','linear');
		}
	});

	/**添加子菜单**/
	$(document).on('click' , '.btn-add-child' ,function(){
		let begin = `<div class="group">`;
		let end = `</div>`;
		$(this).parent().after(begin + strMap.addChild + end);
		spanTransform($(this).siblings('.group-icon') , 'on');
        strMap.handle();
    });

	/***添加同级菜单*/
	$(document).on('click' , '.btn-add' ,function(){
		$(this).parent().parent().after(strMap.add);
        strMap.handle();

	});

	/**删除**/
	$(document).on('click' , '.btn-del' ,function(){
		$(this).parent().parent().remove();
		if($('#form-menu').children('.group').length <= 0){
			$('#form-menu').append(strMap.add);
		}

	});

	/**刷新**/
    $(document).on('click' , '.btn-refresh' ,function(){
        initMenu(apiUrl.index);
    });

    /**取消**/
    $(document).on('click' , '.btn-cancel' ,function(){
        initMenu(apiUrl.index);
    });

	/**旋转icon**/
	function spanTransform(obj , status){
		if(status == 'on'){
			obj.css({
				'transform' : 'rotate(90deg)'
			});
		}else{
			obj.css({
				'transform' : 'rotate(0deg)'
			});
		}
	}

    /**
	 * 渲染数据
     * @param data
     */
	function renderData(obj , data){
		if(data.length > 0){
            obj.children('.group').remove();
			// var renderStr = '';
			$.each(data , function(k , v){
                let renderStr = createStr(v.name , v.url , v.image);
                obj.append(renderStr);
                if(v.hasOwnProperty('child')){
                    spanTransform($(obj).children('.group:last-child').children('.group-row').children('.group-icon') , 'on');
                    renderData( $(obj).children('.group:last-child') , v.child);
				}
			});
		}
	}

    /**
	 * 生成字符串
     * @param name
     * @param url
     * @returns {string}
     */
	function createStr(name , url , image){
        return `<div class="group"><div class="group-row"><span class="glyphicon glyphicon-play group-icon" data-statue="on"></span><input type="text" class="form-control input-text menu-name" placeholder="${__('Name')}" value="${name}"><input type="text" class="form-control input-text menu-url" placeholder="${__('Jump url')}" value="${url}"><button class="btn btn-operate btn-info btn-add">${__('Add')}</button><button class="btn btn-operate btn-danger btn-del">${__('Delete')}</button></div></div>`;
	}


    function getRandomVal(){
        return parseInt(parseInt(Math.random()*(10000000 - 1) + 0,10));
    }


    function handle(){
        $('.btn-upload').each(function(k , v){
            if($(v).attr('data-bind') == 'true'){

            }else{
                let id = $(v).attr('id');
                Upload.api.plupload($('#'+id), function( data, ret ){
                    Toastr.success("成功");
                    $(v).siblings('.menu-image').val(data.url)
                }, function(data, ret){
                    Toastr.success("失败");
                });
                $(v).attr('data-bind' , 'true');
            }

        });
	}

	function initMenu(url , strMapData){
        strMap = strMapData;
        apiUrl.index = url;
        $.ajax({
            url: url,
            data: {},
            type: 'get',
            dataType: 'json',
            success: function (res) {
            	if(res.code == 1){
                    renderData($('#form-menu') , res.rows);
                    strMap.handle();
                }
            },
            error: function (error) {
            }
        });
	}


	return initMenu;

});











