define(['jquery', 'bootstrap', 'backend', 'table', 'form'], function ($, undefined, Backend, Table, Form) {
    var Controller = {
        index:function(){
            require(['backend/home/menu' , 'backend/home/menuAjax' , 'upload'], function(initMenu , saveData , Upload){
                var api = {
                    index_url : 'home/web_menu/getList' + location.search,
                    add_url: 'home/web_menu/addMenu',
                };
                var strMap = {
                    addChild : `<div class="group-row"><span class="glyphicon glyphicon-play group-icon" data-statue="off"></span><input type="text" class="form-control input-text menu-name langAr" placeholder="${__('Name')}"><input type="text" class="form-control input-text menu-url" placeholder="${__('Jump url')}"><button class="btn btn-operate btn-info btn-add">${__('Add')}</button><button class="btn btn-operate btn-danger btn-del">${__('Delete')}</button></div>`,
                    add : `<div class="group"><div class="group-row"><span class="glyphicon glyphicon-play group-icon" data-statue="off"></span><input type="text" class="form-control input-text menu-name langAr" placeholder="${__('Name')}"><input type="text" class="form-control input-text menu-url" placeholder="${__('Jump url')}"><button class="btn btn-operate btn-info btn-add">${__('Add')}</button><button class="btn btn-operate btn-danger btn-del">${__('Delete')}</button></div></div>`,
                    createStr : function (name , url , image , vo) {
                        return `<div class="group"><div class="group-row"><span class="glyphicon glyphicon-play group-icon" data-statue="on"></span><input type="text" class="form-control input-text menu-name langAr" placeholder="${__('Name')}" value="${name}"><input type="text" class="form-control input-text menu-url" placeholder="${__('Jump url')}" value="${url}"><button class="btn btn-operate btn-info btn-add">${__('Add')}</button><button class="btn btn-operate btn-danger btn-del">${__('Delete')}</button></div></div>`;
                    },
                    handle : function(){
                        $('.btn-upload').each(function(k , v){
                            if($(v).attr('data-bind') == 'true'){

                            }else{
                                let id = $(v).attr('id');
                                Upload.api.plupload($('#'+id), function( data, ret ){
                                    Toastr.success("成功");
                                    $(v).siblings('.menu-image').val(data.url)
                                }, function(data, ret){
                                    Toastr.success("失败");
                                });
                                $(v).attr('data-bind' , 'true');
                            }

                        });

                    },
                };

                function getRandomVal(){
                    return parseInt(parseInt(Math.random()*(10000000 - 1) + 0,10));
                }

                initMenu(api.index_url , strMap);
                saveData(api.add_url);
            });
        }
    };
    return Controller;
});