define(['jquery', 'bootstrap', 'backend', 'table', 'form'], function ($, undefined, Backend, Table, Form) {

    var Controller = {
        index: function () {
            // 初始化表格参数配置
            Table.api.init({
                extend: {
                    index_url: 'news/news/index' + location.search,
                    add_url: 'news/news/add' + location.search,
                    edit_url: 'news/news/edit',
                    del_url: 'news/news/del',
                    multi_url: 'news/news/multi',
                    table: 'news',
                }
            });

            var table = $("#table");
            $(".btn-add").data("area", ["100%", "100%"]);
            $(".btn-edit").data("area", ["100%", "100%"]);

            table.on('post-body.bs.table', function (e, settings, json, xhr) {
                $(".btn-editone").data("area", ["100%", "100%"]);
                $(".btn-copy").data("area", ["100%", "100%"]);
            });

            // 初始化表格
            table.bootstrapTable({
                url: $.fn.bootstrapTable.defaults.extend.index_url,
                pk: 'id',
                sortName: 'id',
                columns: [
                    [
                        {checkbox: true},
                        {field: 'id', title: __('News_id')},
                        // {field: 'language_id', title: __('Language_id')},
                        {field: 'category.name', title: __('News_category_id'),formatter:Table.api.formatter.label},
                        {field: 'title', title: __('Title')},
                        {field: 'image', title: __('Image'), events: Table.api.events.image, formatter: Table.api.formatter.image  , operate: false},
                        {field: 'author', title: __('Author')},
                        {field: 'is_external', title: __('Is_external'), searchList: {"1":__('Is_external 1'),"2":__('Is_external 2')}, formatter: Table.api.formatter.normal},
                        {field: 'external_url', title: __('External_url'), formatter: Table.api.formatter.url , operate: false},
                        {field: 'views', title: __('Views'), operate: 'BETWEEN'},
                        {field: 'showtime', title: __('Showtime'), operate:'RANGE', addclass:'datetimerange', formatter: Table.api.formatter.datetime},
                        {field: 'is_open', title: __('Is_open'), searchList: {"1":__('Is_open 1'),"2":__('Is_open 2')}, formatter: Table.api.formatter.toggle},

                        {field: 'buttons', title: __('复制') , width: "80px" , table: table, events: Table.api.events.operate , formatter: Table.api.formatter.buttons , buttons:[
                            {
                                name: 'copy',
                                text: __('复制'),
                                title: __('复制'),
                                classname: 'btn btn-xs btn-primary btn-dialog btn-copy',
                                icon: 'fa fa-folder-o',
                                url: 'news/news/copy',
                                callback: function (data) {
                                    Layer.alert("接收到回传数据：" + JSON.stringify(data), {title: "回传数据"});
                                },
                                visible: function (row) {
                                    //返回true时按钮显示,返回false隐藏
                                    return true;
                                }
                            }
                            ]
                        },
                        // {field: 'createtime', title: __('Createtime'), operate:'RANGE', addclass:'datetimerange', formatter: Table.api.formatter.datetime},
                        // {field: 'updatetime', title: __('Updatetime'), operate:'RANGE', addclass:'datetimerange', formatter: Table.api.formatter.datetime},
                        {field: 'operate', title: __('Operate'), table: table, events: Table.api.events.operate, formatter: Table.api.formatter.operate}
                    ]
                ]
            });

            // 为表格绑定事件
            Table.api.bindevent(table);
        },
        add: function () {
            Controller.api.bindevent();
        },
        edit: function () {
            Controller.api.bindevent();
        },
        copy: function(){

            $('#c-news_category_id').data('params',function(obj){
                return {
                    isTree : 1,
                    isSpace : 1,
                    custom:{
                        language_id: $('#c-afterLanguage').val(),
                    }
                };
            });

            $(document).on('change' , '#c-afterLanguage' , function(e){
                $('#c-news_category_id').selectPageClear()
            });

            Controller.api.bindevent();
        },
        api: {
            bindevent: function () {
                Form.api.bindevent($("form[role=form]"));
            }
        }
    };
    return Controller;
});