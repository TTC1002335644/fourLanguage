define(['jquery', 'bootstrap', 'backend', 'table', 'form'], function ($, undefined, Backend, Table, Form) {

    var Controller = {
        index: function () {
            // 初始化表格参数配置
            Table.api.init({
                extend: {
                    index_url: 'product/product_category/index' + location.search,
                    add_url: 'product/product_category/add',
                    edit_url: 'product/product_category/edit',
                    del_url: 'product/product_category/del',
                    multi_url: 'product/product_category/multi',
                    table: 'product_category',
                }
            });

            var table = $("#table");

            $(".btn-add").data("area", ["100%", "100%"]);
            $(".btn-edit").data("area", ["100%", "100%"]);
            table.on('post-body.bs.table', function (e, settings, json, xhr) {
                $(".btn-editone").data("area", ["100%", "100%"]);
            });

            // 初始化表格
            table.bootstrapTable({
                url: $.fn.bootstrapTable.defaults.extend.index_url,
                pk: 'id',
                sortName: 'weigh',
                sortOrder: 'asc',
                pagination: false,
                escape: false,
                columns: [
                    [
                        {checkbox: true},
                        {field: 'id', title: __('Id')},
                        // {field: 'language_id', title: __('Language_id')},
                        // {field: 'pid', title: __('Pid')},
                        {field: 'name', title: __('Name'),align:"left"},
                        // {field: 'flag', title: __('Flag'), searchList: {"normal":__('Normal'),"new":__('New')}, operate:'FIND_IN_SET', formatter: Table.api.formatter.label},
                        {field: 'image', title: __('Image'), events: Table.api.events.image, formatter: Table.api.formatter.image},
                        {field: 'weigh', title: __('Weigh')},
                        {field: 'is_open', title: __('Is_open'), searchList: {"0":__('Is_open 0'),"1":__('Is_open 1')}, formatter: Table.api.formatter.toggle},
                        {field: 'is_recommend', title: __('Is_recommend'), searchList: {"0":__('Is_open 0'),"1":__('Is_open 1')}, formatter: Table.api.formatter.toggle},
                        {field: 'createtime', title: __('Createtime'), operate:'RANGE', addclass:'datetimerange', formatter: Table.api.formatter.datetime},
                        // {field: 'updatetime', title: __('Updatetime'), operate:'RANGE', addclass:'datetimerange', formatter: Table.api.formatter.datetime},
                        {field: 'operate', title: __('Operate'), table: table, events: Table.api.events.operate, formatter: Table.api.formatter.operate}
                    ]
                ]
            });

            // 为表格绑定事件
            Table.api.bindevent(table);
        },
        add: function () {
            Controller.api.bindevent();
        },
        edit: function () {
            $(document).on('click' , '.del-attribute' , function(){
                $(this).parents('.attribute-list').find('.del-val').val(1)
                $(this).parents('.attribute-list').hide();
            });


            Controller.api.bindevent();
        },
        api: {
            bindevent: function () {
                $(document).on('change' , '#c-pid' , function(){
                    if($(this).val() != 0){
                        $('.hide-list').hide();
                    }else{
                        $('.hide-list').show();
                    }
                });


                Form.api.bindevent($("form[role=form]"));
            }
        }
    };
    return Controller;
});