define(['jquery', 'bootstrap', 'backend', 'table', 'form'], function ($, undefined, Backend, Table, Form) {

    var Controller = {
        index: function () {
            // 初始化表格参数配置
            Table.api.init({
                extend: {
                    index_url: 'product/product_attribute/index' + location.search,
                    add_url: 'product/product_attribute/add',
                    edit_url: 'product/product_attribute/edit',
                    del_url: 'product/product_attribute/del',
                    multi_url: 'product/product_attribute/multi',
                    table: 'product_attribute',
                }
            });

            var table = $("#table");
            $(".btn-add").data("area", ["100%", "100%"]);
            $(".btn-edit").data("area", ["100%", "100%"]);
            table.on('post-body.bs.table', function (e, settings, json, xhr) {
                $(".btn-editone").data("area", ["100%", "100%"]);
            });

            // 初始化表格
            table.bootstrapTable({
                url: $.fn.bootstrapTable.defaults.extend.index_url,
                pk: 'id',
                sortName: 'id',
                columns: [
                    [
                        {checkbox: true},
                        {field: 'id', title: __('Id')},
                        {field: 'name', title: __('Name')},
                        {field: 'createtime', title: __('Createtime'), operate:'RANGE', addclass:'datetimerange', formatter: Table.api.formatter.datetime},
                        // {field: 'updatetime', title: __('Updatetime'), operate:'RANGE', addclass:'datetimerange', formatter: Table.api.formatter.datetime},
                        {field: 'operate', title: __('Operate'), table: table, events: Table.api.events.operate, formatter: Table.api.formatter.operate}
                    ]
                ]
            });

            // 为表格绑定事件
            Table.api.bindevent(table);
        },
        add: function () {
            Controller.api.bindevent();
        },
        edit: function () {
            Controller.api.bindevent();
        },
        api: {
            bindevent: function () {


                var attrStr = `<div class="form-group">
        <label class="control-label col-xs-12 col-sm-2">${__('Attribute')}:</label>
        <div class="col-xs-3 col-sm-2">
            <input id="c-attribute-name" class="form-control" name="row[attribute-name][]" type="text" value="" placeholder="${__('Attribute name')}">
        </div>
        <div class="col-xs-7 col-sm-5">
            <input id="c-attribute-value" class="form-control" name="row[attribute-value][]" type="text" value="" placeholder="${__('Attribute value')}">
        </div>
        <div class="col-xs-2 col-sm-2">
            <button type="button" class="btn btn-warning add-attribute" >${__('Add')}</button>
            <button type="button" class="btn btn-danger del-attribute" >${__('Del')}</button>
        </div>
    </div>`;

                $(document).on('click' , '.add-attribute' , function(){
                    $(this).parents('.form-group').after(attrStr);
                });

                $(document).on('click' , '.del-attribute' , function(){
                    if($('.del-attribute').length <= 1){
                        $(this).parents('.form-group').find('input').val('')
                    }else{
                        $(this).parents('.form-group').remove();
                    }
                });




                Form.api.bindevent($("form[role=form]"));
            }
        }
    };
    return Controller;
});

