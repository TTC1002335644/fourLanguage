define(['jquery', 'bootstrap', 'backend', 'table', 'form'], function ($, undefined, Backend, Table, Form) {

    var Controller = {
        index: function () {
            // 初始化表格参数配置
            Table.api.init({
                extend: {
                    index_url: 'product/product/index' + location.search,
                    add_url: 'product/product/add',
                    edit_url: 'product/product/edit',
                    del_url: 'product/product/del',
                    multi_url: 'product/product/multi',
                    table: 'product',
                }
            });

            var table = $("#table");
            $(".btn-add").data("area", ["100%", "100%"]);
            $(".btn-edit").data("area", ["100%", "100%"]);
            table.on('post-body.bs.table', function (e, settings, json, xhr) {
                $(".btn-editone").data("area", ["100%", "100%"]);
                $(".btn-copy").data("area", ["100%", "100%"]);
            });


            // 初始化表格
            table.bootstrapTable({
                url: $.fn.bootstrapTable.defaults.extend.index_url,
                pk: 'id',
                sortName: 'id',
                pageList: [10, 25, 50 , 100 , 'All'],
                columns: [
                    [
                        {checkbox: true},
                        {field: 'id', title: __('Product_id')},
                        // {field: 'language_id', title: __('Language_id')},
                        // {field: 'product_category', title: __('Product_category')},
                        {field: 'category.name', title: __('Product_category') , operate:false},
                        {field: 'title', title: __('Title'),operate:'LIKE'},
                        {field: 'none', title: __('Product_category first') , visible:false , searchList: $.getJSON("product/product_category/getSecondCategory?language_id="+language_id)},
                        {field: 'product_category', title: __('Product_category second') , visible:false , searchList:{} },
                        {field: 'color.color_id', title: __('Color') , searchList:{},visible: false},
                        {field: 'weight', title: __('Weight') , searchList:{},visible: false},
                        {field: 'weights.attribute_name', title: __('Weight'),operate:false},
                        {field: 'cover_image', title: __('Cover_image'), events: Table.api.events.image, formatter: Table.api.formatter.image , operate:false},
                        {field: 'views', title: __('Views'), operate: 'BETWEEN'},
                        {field: 'added_time', title: __('Added_time'), operate:'RANGE', addclass:'datetimerange', formatter: Table.api.formatter.datetime},
                        {field: 'is_open', title: __('Is_open'), searchList: {"1":__('Is_open 1'),"2":__('Is_open 2')}, formatter: Table.api.formatter.normal},
                        {field: 'buttons', title: __('复制') , width: "80px" , table: table, events: Table.api.events.operate , formatter: Table.api.formatter.buttons , buttons:[
                                {
                                    name: 'copy',
                                    text: __('复制'),
                                    title: __('复制'),
                                    classname: 'btn btn-xs btn-primary btn-dialog btn-copy',
                                    icon: 'fa fa-folder-o',
                                    url: 'product/product/copy',
                                    callback: function (data) {
                                        Layer.alert("接收到回传数据：" + JSON.stringify(data), {title: "回传数据"});
                                    },
                                    visible: function (row) {
                                        //返回true时按钮显示,返回false隐藏
                                        return true;
                                    }
                                }
                            ]
                        },
                        {field: 'operate', title: __('Operate'), table: table, events: Table.api.events.operate, formatter: Table.api.formatter.operate}
                    ]
                ]
            });

            $('.commonsearch-table select[name="none"]').change(function(){
                let categoryId = $(this).val();
                $.getJSON("product/product_category/getSecondCategory?language_id="+language_id+'&pid='+categoryId ,function(result){
                    // $('.commonsearch-table select[name="color"]').empty();
                    var secondObj = $('.commonsearch-table select[name="product_category"]')
                    secondObj.empty().append(`<option value>选择</option>`);
                    $.each(result, function(i, v){
                        secondObj.append(
                            `<option value="${v.id}">${v.name}</option>`
                        );
                    })
                });

                $('.commonsearch-table select[name="color.color_id"]').empty().append(`<option value>选择</option>`);
                $('.commonsearch-table select[name="weight"]').empty().append(`<option value>选择</option>`);

                $.getJSON("product/category_color_relation/getColorCategory?category_id="+categoryId ,function(result){
                    // $('.commonsearch-table select[name="color"]').empty();
                    var colorObj = $('.commonsearch-table select[name="color.color_id"]')
                    colorObj.empty().append(`<option value>选择</option>`);
                    $.each(result, function(i, v){
                        colorObj.append(
                            `<option value="${v.id}">${v.color_name}</option>`
                        );
                    })
                });

                $.getJSON("product/category_attribute_relation/getAttributeCategory?category_id="+categoryId ,function(result){
                    var weightObj = $('.commonsearch-table select[name="weight"]')
                    weightObj.empty().append(`<option value>选择</option>`);
                    $.each(result, function(i, v){
                        weightObj.append(
                            `<option value="${v.id}">${v.attribute_name}</option>`
                        );
                    })
                });
            });


            // 为表格绑定事件
            Table.api.bindevent(table);
        },
        add: function () {
            $('#c-color').data('params',function(obj){
                return {custom:{language_id: language_id,product_category_id:$( '#c-product_category').val()}};
            });

            $('#c-weight').data('params',function(obj){
                return {
                    custom:{
                        language_id: language_id,
                        product_category_id:$( '#c-product_category').val()
                    }
                };
            });

            $(document).on('change' , '#c-product_category' , function(e){
                $('#c-color').selectPageClear()
                $('#c-weight').selectPageClear()
            });
            Controller.api.bindevent();
        },
        edit: function () {
            // $('#c-color').data('params',function(obj){
            //     return {custom:{language_id: language_id,product_category_id:init_category}};
            // });
            //
            // $('#c-weight').data('params',function(obj){
            //     return {custom:{language_id: language_id,product_category_id:init_category}};
            // });

            $('#c-color').data('params',function(obj){
                return {custom:{language_id: language_id,product_category_id:$( '#c-product_category').val()}};
            });

            $('#c-weight').data('params',function(obj){
                return {
                    custom:{
                        language_id: language_id,
                        product_category_id:$('#c-product_category').val()
                    }
                };
            });

            $(document).on('change' , '#c-product_category' , function(e){
                $('#c-color').selectPageClear()
                $('#c-weight').selectPageClear()
            });
            Controller.api.bindevent();
        },
        copy: function(){

            $('#c-product_category').data('params',function(obj){
                return {
                    isTree : 1,
                    isSpace : 1,
                    custom:{
                        language_id: $('#c-afterLanguage').val(),
                    }
                };
            });



            $('#c-color').data('params',function(obj){
                return {custom:{language_id: $('#c-afterLanguage').val() , product_category_id:$('#c-product_category').val()}};
            });

            $('#c-weight').data('params',function(obj){
                return {
                    custom:{
                        language_id: $('#c-afterLanguage').val(),
                        product_category_id:$( '#c-product_category').val()
                    }
                };
            });

            $(document).on('change' , '#c-afterLanguage' , function(e){
                $('#c-product_category').selectPageClear()
                $('#c-color').selectPageClear()
                $('#c-weight').selectPageClear()
            });

            $(document).on('change' , '#c-product_category' , function(e){
                $('#c-color').selectPageClear()
                $('#c-weight').selectPageClear()
            });

            Controller.api.bindevent();
        },
        api: {
            bindevent: function () {
                // $('#c-color').data('params',function(obj){
                //     return {custom:{language_id: language_id,product_category_id:$( '#c-product_category').val()}};
                // });
                //
                // $('#c-weight').data('params',function(obj){
                //     return {
                //         custom:{
                //             language_id: language_id,
                //             product_category_id:$( '#c-product_category').val()
                //         }
                //     };
                // });
                //
                // $(document).on('change' , '#c-product_category' , function(e){
                //     $('#c-color').selectPageClear()
                //     $('#c-weight').selectPageClear()
                // });

                Form.api.bindevent($("form[role=form]"));
            }
        }
    };
    return Controller;
});
