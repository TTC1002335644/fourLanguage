const menuStr = `
			<nav class="navbar navbar-static-top navbar-default">
				<div class="container-fluid">
					<div class="navbar-header">
						<a class="logo" href="index.html"><img src="images/logo.png"></a>
					<div class="navbar_rightbox">
					<select class="form-control">
					<option>选择语言</option>
					<option>中文</option>
					<option>英文</option>
					<option>西班牙语</option>
					<option>阿拉伯语</option>
				</select>
				<button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#navbar" aria-expanded="false"
				 aria-controls="navbar">
					<span class="sr-only">Toggle navigation</span>
					<span class="icon-bar"></span>
					<span class="icon-bar"></span>
					<span class="icon-bar"></span>
				</button>
					
					</div>
					</div>
				

					<div class="language-list">
						<ul class="country-list">
							
							<li><img src="img/en.png" /></li>
							<li><img src="img/sp.png" /></li>
							<li><img src="img/ar.png" /></li>
							<li><img src="img/zh_cn.png" /></li>
						</ul>
					</div>

					<div id="navbar" class="collapse navSelect navbar-collapse">
						<ul class="nav navbar-nav nav-menu">
							 <li><a href="index.html">首页</a></li> 
							<li>
								<a href="about.html">关于我们</a>
							</li>

							<li>
								<a href="product.html" >产品中心</a>
							</li>

							<li>
								<a href="news.html">新闻中心</a>
							</li>
							<li><a href="download.html">资料中心</a></li>
							<li><a id="turn-6" href="index.html#6">销售网络</a></li>
							<li><a href="contact.html">联系我们</a></li>
						</ul>
					</div>


				</div>
			</nav>
		`;


const footerStr = `<div class="copyright">
            <p>
                <a href="about.html">公司简介</a>&nbsp;&nbsp;|&nbsp;&nbsp;
                <a href="news.html">新闻中心</a>&nbsp;&nbsp;|&nbsp;&nbsp;
                <a href="product.html">产品中心</a>&nbsp;&nbsp;|&nbsp;&nbsp;
                <a href="contact.html">联系我们</a>
            </p>
            <p class="copyright_p">© 2000-2017 MojoCube All Rights Reserved. <a href="http://www.baidu.com/" target="_blank">百度一下</a></p>
        </div>`;


$('.main-menu').empty().append(menuStr)

$('footer').empty().append(footerStr)