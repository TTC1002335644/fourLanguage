$(document).on('click' , '.area-lists li' , function(){
	$('.area-lists li').removeClass('area-on')
	$(this).addClass('area-on')
	$('.area-lists li i').removeClass('icon-active')
	$(this).children('i').addClass('icon-active')
	let liIndex = $(this).index()
	$('.area-country-lists.country-on').removeClass('country-on')
	$('.area-country-lists').eq(liIndex).addClass('country-on')
	
})

$(document).on('click' , '.area-country-lists li' ,function(){
	$('.area-country-lists li').removeClass('country-li-on')
	$(this).addClass('country-li-on')
	
	let index = $('.area-lists li').index($('.area-on'))


	let secondIndex = $(this).index()

	// let areaImage = $(this).attr('data-image')
	//
	// if(areaImage.length != 0){
	// 	$('.left-logo img').attr('src' , areaImage);
	// }

	var contactRes = createContactStr(index , secondIndex)
	initContact(contactRes)

})



function initContact(contactRes){
	$('.contact-info ul').empty().append(contactRes)
	$('#area-content').show()

}




function createContactStr(index = 0 , secondIndex = 0){
	let data = mapData[index][secondIndex]
	var res = '';
	if(Array.isArray(data)){
		try{
			if(data.length > 2){
				$('.time-arrow').show()
			}else{
                $('.time-arrow').hide()
			}
		}catch (e) {

        }
		data.forEach(function(v){
			res = res + getStr(v.name , v.phone , v.mail , v.wechat)
		})
	}else{
		res = getStr(data.name , data.phone , data.mail , data.wechat)
	}
	return res
}


function getStr(contact = '' , tel = '' , eamil = '' , wechat = ''){
	if(langCode == 'ar'){
        var str = `<li>
					<p>${contact}：${ContactPersonTitle}</p>
					<p>${tel}：${PhoneTitle}</p>
					<p>${eamil}：${MailboxTitle}</p>
					<p>${wechat}：${WeChatTitle}</p>
				</li>`
	}else{
        var str = `<li>
					<p>${ContactPersonTitle}：${contact}</p>
					<p>${PhoneTitle}：${tel}</p>
					<p>${MailboxTitle}：${eamil}</p>
					<p>${WeChatTitle}：${wechat}</p>
				</li>`
	}

				
	return str			
}

// initContact(createContactStr(0,0))