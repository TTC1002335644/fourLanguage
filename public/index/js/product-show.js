const productBannerLiWidth = $('.product-banner li').width();
var start = 0;
var totalWidth =  $('.product-banner li').length * productBannerLiWidth;

//上一个
$('.product-banner-prev').click(function() {
    getLiObject('prev')
    if(start + productBannerLiWidth < productBannerLiWidth){
        start = start + productBannerLiWidth
        $('#product-images').css({
            transform: "translateX(" + start + "px)",
            transition: "transform 1s"
        });
    }

})

//下一个
$('.product-banner-next').click(function() {
    getLiObject('next')

    if(Math.abs(start) < (totalWidth - 2 * productBannerLiWidth)){
        console.log(start)
        start = start - productBannerLiWidth;
        $('#product-images').css({
            transform: "translateX(" + start + "px)",
            transition: "transform 0.5s"
        });
    }

})

$('.product-banner li').click(function() {
    let className = 'on'
        //获取当前的index
    let currentObj = $('.product-banner li.' + className)
    changeClass(currentObj, $(this), className)
})


$('.product-banner-show').hover(function(){
    let photosData = {
        title: "",
        id: 123,
        move  : false,
        data: [
            {
                "alt": "",
                "src": $('.product-banner-show').attr('src'),
                "thumb": $('.product-banner-show').attr('src')
            }
        ]
    }
    layer.photos({
        photos: photosData
        ,anim: 5
    });
    $('.layui-layer-photos').mouseout(function(){
        layer.closeAll();
    })
});



function getLiObject(type = '') {
    // alert(type)
    let className = 'on'
        //获取当前的index
    let currentObj = $('.product-banner li.' + className)

    if (type == 'prev') {
        var changeObj = currentObj.prev()
    } else if (type == 'next') {
        var changeObj = currentObj.next()
    } else {
        return;
    }

    if (changeObj.length == 0) {
        return;
    }
    changeClass(currentObj, changeObj, className)
}

function changeClass(current, change, className = 'on') {
    current.removeClass(className)
    change.addClass(className)
    $('.product-banner-show').attr('src', change.children('img').attr('src'))
}

function moveList(type = '') {
    var obj = $('.product-banner')
    var allWidth = obj.width()
    if (type == 'prev') {
        var width = productBannerLiWidth
    } else if (type == 'next') {
        var width = -1 * productBannerLiWidth
    } else {
        return;
    }



    obj.css({
        transform: "translateX(" + width + "px)",
    });
}