var sleep = function(time) {
    var startTime = new Date().getTime() + parseInt(time, 10);
    while(new Date().getTime() < startTime) {}
};

sleep(1000);

$('.crops__item-image').each(function(){
    let width = $(this).width()
    $(this).css("width", width);
    $(this).css("height", width * 0.8);
})

